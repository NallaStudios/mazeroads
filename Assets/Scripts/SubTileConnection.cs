﻿using UnityEngine;
using System.Collections;

public class SubTileConnection : MonoBehaviour {
	
	public int m_ConnectionGap;
	
	public int m_DisableSubtileCount;
	
	public SubTile.SubTileType m_ConnectsSubTileType;
	
	private Index m_Connection1;
	private Index m_Connection2;
	
	public Index Connection1
	{
		get
		{
			return m_Connection1;
		}
		set
		{
			m_Connection1 = value;
		}
	}
	
	public Index Connection2
	{
		get
		{
			return m_Connection2;
		}
		set
		{
			m_Connection2 = value;
		}
	}

	// Use this for initialization
	protected virtual void Start () {
	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
	
	}
}
