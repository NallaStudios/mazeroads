﻿using UnityEngine;
using System.Collections;


public class Tile : MonoBehaviour {
	
	public enum Type
	{
		eNone,
		eStatic,
		eDefault,
	}
	
	public enum OrientationType
	{
		eOrientation_0 = 0,
		eOrientation_90,
		eOrientation_180,
		eOrientation_270,
	}
	
	public static readonly float[] Angles = {0.0f,90.0f,180.0f,270.0f};
	
	public static readonly int SubTileRowCount = 3;
	public static readonly int SubTileColCount = 3;	
		
	public SubTile[] m_SubTiles;
	
	private Index m_Index;
	private Board.TileData m_TileData;
	private SubTile[] m_GameSubTiles;
	
	private LevelBoard m_LevelBoard;
	
	public SubTile[] GameSubTiles
	{
		get
		{
			return m_GameSubTiles;
		}
	}
	
	public Board.TileData TileData
	{
		get
		{
			return m_TileData;
		}
		set
		{
			m_TileData = value;
		}
	}
	
	public OrientationType Orientation
	{
		get
		{
			return m_TileData.m_Orientation;
		}
		set
		{
			m_TileData.m_Orientation = value;
		}
	}
	
	public Type TileType
	{
		get
		{
			return m_TileData.m_TileType;
		}
		set
		{
			m_TileData.m_TileType = value;
		}
	}
	
	public Index Index
	{
		get
		{
			return m_Index;
		}
		set
		{
			m_Index = value;
		}
	}
	
	public LevelBoard CurrentLevelBoard
	{
		get
		{
			return m_LevelBoard;
		}
		set
		{
			m_LevelBoard = value;
		}
	}
	
	public void UpdateSubTileIndices()
	{
		for (int i = 0 ; i < SubTileRowCount ; i++)
		{
			for (int j = 0 ; j < SubTileColCount ; j++)
			{
				m_SubTiles[i * SubTileColCount + j].LocalIndex = new Index(i,j);
			}
		}
	}

	public void UpdateTileBasedOnOrientation(bool updateAngle)	{
		
		if (updateAngle)
			transform.localRotation = Quaternion.Euler(new Vector3(0.0f,RotationAngle(),0.0f));
			
		if (m_GameSubTiles == null )
			m_GameSubTiles = new SubTile[SubTileRowCount * SubTileColCount];
			
		for (int i = 0 ; i < SubTileRowCount ; i++)
		{
			for (int j = 0 ; j < SubTileColCount ; j++)
			{
				int destIndex;
				int sourceIndex;
				
				GetSubtileRotateData(i,j,out destIndex,out sourceIndex);
				m_GameSubTiles[destIndex] = m_SubTiles[sourceIndex];
				if (m_GameSubTiles[destIndex].LocalIndex == null)
					m_GameSubTiles[destIndex].LocalIndex = new Index();
				
				m_GameSubTiles[destIndex].LocalIndex.row = i;
				m_GameSubTiles[destIndex].LocalIndex.col = j;
			}
		}

	}
	
	public void CreateItems(Board.TileData boardTileData)
	{
		if (boardTileData.m_Items == null || boardTileData.m_Items.Length == 0)
			return;
		
		if (m_TileData.m_Items == null )
			m_TileData.m_Items = new Board.SubTileItemData[boardTileData.m_Items.Length];
				

		foreach (Board.SubTileItemData itemData in boardTileData.m_Items)
		{
			SubTile subTile = GameSubTiles[itemData.m_LocalIndex.row * SubTileColCount + itemData.m_LocalIndex.col];
			GameObject newItem = Instantiate(itemData.m_Item.gameObject) as GameObject;
			newItem.transform.parent = subTile.transform.parent;
			
			SubTileItem itemComp = newItem.GetComponent<SubTileItem>();
			subTile.AddSubTileItem(itemComp);
			itemComp.Parent = subTile;
			itemComp.CurrentLevelBoard = CurrentLevelBoard;
			
			newItem.transform.position = subTile.transform.position;
		}
	}
	
	public void ClearAllSubTileItems()
	{
		foreach (SubTile subtile in m_TileData.m_Tile.GameSubTiles)
		{
			if (subtile.SubTileItems != null)
			{
				foreach (SubTileItem currItem in subtile.SubTileItems)
				{
					if (currItem.gameObject != null)
						GameObject.Destroy(currItem.gameObject);
				}
				subtile.SubTileItems.Clear();
			}
		}
	}

	
	private void GetSubtileRotateData(int row, int col, out int destIndex, out int sourceIndex)
	{
		destIndex = row * SubTileColCount + col;
		sourceIndex = destIndex;
		
		switch(Orientation)
		{
		case OrientationType.eOrientation_0:
			break;
		
		case OrientationType.eOrientation_90:
			destIndex = row * SubTileColCount + col;
			sourceIndex = (SubTileColCount-col-1) * SubTileRowCount + row;
			break;
			
		case OrientationType.eOrientation_180:
			destIndex = row * SubTileColCount + col;
			sourceIndex = (SubTileRowCount-row-1) * SubTileColCount + (SubTileColCount-col-1);
			break;
			
		case OrientationType.eOrientation_270:
			destIndex = row * SubTileColCount + col;
			sourceIndex = col * SubTileRowCount + (SubTileRowCount-row-1);
			break;
		}
	}
	
	private float RotationAngle()
	{
		switch(Orientation)
		{
		case OrientationType.eOrientation_0:
			return 0.0f;			
					
		case OrientationType.eOrientation_90:
			return 90.0f;
			
		case OrientationType.eOrientation_180:
			return 180.0f;
			
		case OrientationType.eOrientation_270:
			return 270.0f;
		}
		return 0.0f;
	}
	
	public bool HasConnection()
	{
		foreach (SubTile subtile in GameSubTiles)
		{
			if (subtile.AllConnections != null && subtile.AllConnections.Count > 0)
				return true;
		}
		return false;
	}
	
	public Index GetSubTileBoardIndex(SubTile subtile)
	{
		Index newIndex = new Index();
		
		newIndex.row = Index.row * Tile.SubTileRowCount + subtile.LocalIndex.row;
		newIndex.col = Index.col * Tile.SubTileColCount + subtile.LocalIndex.col;
		
		return newIndex;
	}
}
