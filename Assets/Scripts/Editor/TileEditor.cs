﻿using UnityEngine;
using UnityEditor;
using System.Collections;


//[CustomEditor(typeof(Tile))] 
public class TileEditor : Editor {
	
	private static Tile m_SelectedTile;
	private static SubTile m_SelectedSubTile;
	private Tool m_LastTool = Tool.None;
	private bool m_ShowSubtiles = true;
	
	private Texture2D m_PreviewTexture;
	
	void OnEnable () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		
		Tools.current = Tool.None;
		
		GameObject selectedGO = Selection.activeObject as GameObject;
		if (selectedGO != null)
			m_SelectedTile = selectedGO.GetComponent<Tile>();
				
		//SelectSubTile();
		Repaint();
	}
	
	void OnDisable () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		GameObject selected = Selection.activeObject as GameObject;
		
		m_SelectedTile = null;
		
		if (selected != null && selected.transform != null)
		{
			m_SelectedSubTile = selected.GetComponent<SubTile>();
			
			Transform parent = selected.transform.parent;
			
			while(parent != null)
			{
				GameObject parentGO = parent.gameObject;
				
				if (parentGO != null)
				{
					Tile tile = parentGO.GetComponent<Tile>();
					
					if (tile != null)
					{
						m_SelectedTile = tile;
				
						break;
					}
				}
				
				parent = parent.parent;
			}
		}
		
		if (m_SelectedTile != null)
		{
			Selection.activeObject = m_SelectedTile;
		}
		else
		{
			m_SelectedTile = null;
			m_SelectedSubTile = null;
		}
		
		Tools.current = m_LastTool;
	}
	
	public void OnSceneGUI () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		
		if (Event.current.type == EventType.MouseUp)
		{
			SubTile lastSelected = m_SelectedSubTile;
			SelectSubTile();
			
			if (lastSelected != m_SelectedSubTile)
				Repaint();
		}
		
		if (m_SelectedSubTile != null)
		{
			Handles.color = Color.red;
			
			HandleUtility.Repaint();
			HandleUtility.AddDefaultControl (controlID);
			
			GameObject tileSelected = m_SelectedSubTile.gameObject;
			Handles.SelectionFrame(controlID,tileSelected.transform.position,tileSelected.transform.localRotation,0.5f);
			
			if (Event.current.type == EventType.MouseDrag) 
			{
				DragAndDrop.PrepareStartDrag ();
				
				DragAndDrop.objectReferences = new UnityEngine.Object[] {m_SelectedSubTile};
	
				// Start the actual drag
				DragAndDrop.StartDrag ("drag");
				
				Event.current.Use();	 
			}
		}
		
	}

	private void SelectSubTile() {

		GameObject go = HandleUtility.PickGameObject(Event.current.mousePosition,false);
		if (go != null)
		{
			SubTile subtile = go.GetComponent<SubTile>();
			if (subtile != null)
			{
				if (go.transform.parent == m_SelectedTile.transform)
				{
					m_SelectedSubTile = subtile;
				}
				else
					m_SelectedSubTile = null;
			}
			else
				m_SelectedSubTile = null;
		}
		else
			m_SelectedSubTile = null;
		
		if (m_SelectedSubTile == null)
			Selection.activeObject = null;
	}
	
	public override void OnInspectorGUI () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
	
		
		if (m_SelectedTile != null && m_SelectedTile.m_SubTiles != null) 
		{		
			Material newMat = null;
			if (m_SelectedSubTile != null)
			{
				newMat = EditorGUILayout.ObjectField("Material",m_SelectedSubTile.gameObject.GetComponent<Renderer>().sharedMaterial,typeof(Material),true) as Material;
			
				m_PreviewTexture = AssetPreview.GetAssetPreview(newMat.mainTexture);
								
				if (m_PreviewTexture != null)
				{
					EditorGUILayout.BeginHorizontal();
						EditorGUILayout.Space();
						GUILayout.Label(m_PreviewTexture);
					EditorGUILayout.EndHorizontal();
				}
				
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				
				ShowSubtile(m_SelectedSubTile);
			}
			
			if (newMat != null && newMat != m_SelectedSubTile.gameObject.GetComponent<Renderer>().sharedMaterial)
			{
				m_SelectedSubTile.gameObject.GetComponent<Renderer>().sharedMaterial = newMat;
			}
			
			EditorGUILayout.Separator();
			
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			
			m_ShowSubtiles = EditorGUILayout.Foldout(m_ShowSubtiles, "Sub tiles");
			
			if (m_ShowSubtiles)
			{
			    for(int i = 0; i < m_SelectedTile.m_SubTiles.Length; i++) 
				{
			        m_SelectedTile.m_SubTiles[i] = EditorGUILayout.ObjectField("SubTile "+i, m_SelectedTile.m_SubTiles[i], typeof(SubTile),true) as SubTile;
					if (Event.current.type == EventType.MouseUp)
					{
						if (GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition )) 
						{
							if (m_SelectedTile.m_SubTiles[i] != null)
							{
								m_SelectedSubTile = m_SelectedTile.m_SubTiles[i];
								Repaint();
							}
						}
					}
					
					EditorGUILayout.BeginHorizontal("TextArea");
						EditorGUILayout.Space();				
						m_SelectedTile.m_SubTiles[i].m_SubTileType = (SubTile.SubTileType)EditorGUILayout.EnumPopup("SubTileType",m_SelectedTile.m_SubTiles[i].m_SubTileType);
						EditorGUILayout.Space();
					EditorGUILayout.EndHorizontal();
					
					EditorGUILayout.Space();
					EditorGUILayout.Space();
				}
		    }
		}
	}
	
	private void ShowSubtile(SubTile subtile)
	{
		subtile = EditorGUILayout.ObjectField("Selected ", subtile, typeof(SubTile),true) as SubTile;
		
		EditorGUILayout.BeginHorizontal();			
			subtile.m_SubTileType = (SubTile.SubTileType)EditorGUILayout.EnumPopup("SubTileType",subtile.m_SubTileType);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}
	
}
