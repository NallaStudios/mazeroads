﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

[CustomEditor(typeof(LevelBoard))] 
public class LevelEditor : Editor {
	

	private ArrayList m_AllTileTypes;
	private static GameObject m_LevelGO;
	private static Index m_TileSelected;
	private ArrayList m_TileTypeItems;
	
	private GameObject m_SelectedGizmoGO;
	private GameObject m_SubTileGizmoGO;
	private Index m_SelectedSubTile;
	
	private LevelBoard m_Level;
	
	private Tool m_LastTool = Tool.None;
	
	//private ArrayList m_AllItemsType;
	private string[] m_AllItemsNames;
	private ArrayList m_ItemTypes;
	private static int m_CurrItem = 0;
	
	void OnEnable () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		UnityEngine.Object[] assets = Resources.LoadAll("Prefabs/Tiles",typeof(GameObject));
		
		m_AllTileTypes = new ArrayList();
		m_TileTypeItems = new ArrayList();
		
		for (int i = 0 ; i < assets.Length ; i++)
		{
			PrefabType type = PrefabUtility.GetPrefabType(assets[i]); 
			GameObject go = assets[i] as GameObject;
			Tile tileComp = go.GetComponent<Tile>();
			if (type == PrefabType.Prefab && tileComp != null)
			{
				m_AllTileTypes.Add(tileComp);
			}
			
			if(go.transform.name == "TileNone")
			{
				Tile temp = m_AllTileTypes[0] as Tile;
				m_AllTileTypes[0] = tileComp;
				m_AllTileTypes[i] = temp;
			}
		}
		
		for (int i = 0 ; i < (int)m_AllTileTypes.Count ; i++ )
		{
			m_TileTypeItems.Add( (m_AllTileTypes[i] as Tile).gameObject.transform.name );
		}
		
		m_LevelGO = Selection.activeObject as GameObject;
		
		if (m_LevelGO == null)
		{
			Selection.activeObject = null;
			return;
		}
		
		m_Level = m_LevelGO.GetComponent<LevelBoard>();
			
		UpdateBoard(m_Level.m_CurrentBoard,m_Level.m_CurrentBoard.m_RowCount,m_Level.m_CurrentBoard.m_ColCount);
		
		m_LastTool = Tools.current;

        Tools.current = Tool.None;
		
		//m_AllItemsPrefabs = Object.FindObjectsOfType(typeof(SubTileItem));
		//m_AllItemsType.
		
		/*System.Type[] allTypes = Assembly.GetAssembly(typeof(SubTileItem)).GetTypes();//typeof(SomeClassOrInterface).IsAssignableFrom
		//List<System.Type> allTypesList = new List<System.Type>(allTypes);
		
		m_AllItemsType = new ArrayList();
		
		for (int i = 0 ; i < allTypes.Length ; i++)
		{
			if (typeof(SubTileItem) != allTypes[i] && typeof(SubTileItem).IsAssignableFrom(allTypes[i]))
			{
				m_AllItemsType.Add(allTypes[i]);
			}
		}
		
		
		if (m_AllItemsType != null)
		{
			m_AllItemsNames = new string[m_AllItemsType.Count];
			
			for (int i = 0 ; i < m_AllItemsType.Count ; i++)
			{
				System.Type type = m_AllItemsType[i] as System.Type;
				m_AllItemsNames[i] = type.Name;
			}
		}*/
		
		Object[] allItems = Resources.LoadAll("Prefabs/Items",typeof(GameObject));
		
		m_ItemTypes = new ArrayList();
		
		for (int i = 0 ; i < allItems.Length ; i++)
		{
			GameObject currItem = allItems[i] as GameObject;
			
			if (currItem != null)
			{
				SubTileItem item = currItem.GetComponent<SubTileItem>();
				
				if (item != null)
					m_ItemTypes.Add(currItem);
			}
		}
		
		m_AllItemsNames = new string[m_ItemTypes.Count];
			
		for (int i = 0 ; i < m_ItemTypes.Count ; i++)
		{
			m_AllItemsNames[i] = (m_ItemTypes[i] as GameObject).name;
		}
		
    }

	
	void OnDisable () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		if (m_SubTileGizmoGO != null && m_SubTileGizmoGO == Selection.activeObject)
		{
			GameObject.DestroyImmediate(m_SubTileGizmoGO);
			Selection.activeObject = m_LevelGO;
			m_TileSelected = null;
			m_SelectedSubTile = null;
			
			if (m_SelectedGizmoGO != null)
				GameObject.DestroyImmediate(m_SelectedGizmoGO);
			
			return;
		}
		else
		{
			if (m_SubTileGizmoGO != null)
				GameObject.DestroyImmediate(m_SubTileGizmoGO);
			
			m_SelectedSubTile = null;
		}
		
		m_TileSelected = null;
		GameObject levelGO = Selection.activeObject as GameObject;
		
		bool levelFound = false;
		
		if (levelGO != null /*&& Application.isEditor*/)
		{
			Tile tile = m_LevelGO.GetComponent<Tile>();
			if (tile != null)
			{
				Selection.activeObject = m_LevelGO;
				m_TileSelected = tile.Index;
				levelFound = true;
			}
			else
			{
				Transform trans = levelGO.transform;
				if (trans != null)
				{
					Transform parentTrans = trans.parent;
						
					if (parentTrans != null)
					{
						tile = parentTrans.gameObject.GetComponent<Tile>();
						if (tile != null)
						{
							Selection.activeObject = m_LevelGO;
							m_TileSelected = tile.Index;
							levelFound = true;
						}
					}
				}
			}
		}
		
		if (!levelFound)
		{
			if (m_SelectedGizmoGO != null)
				GameObject.DestroyImmediate(m_SelectedGizmoGO);
			GameObject.DestroyImmediate(m_LevelGO);
			m_LevelGO = null;
			m_TileSelected = null;
		}
		
		Tools.current = m_LastTool;
	}
	
	
	public void OnSceneGUI () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		
		if (Event.current.type == EventType.MouseUp)
		{
			int button = Event.current.button;
			
			if (button == 1)
			{
				m_TileSelected = null;
				if (m_SelectedGizmoGO != null)
					GameObject.DestroyImmediate(m_SelectedGizmoGO);
				
				
				SelectSubTile();
				
				Object obj = Resources.Load("Prefabs/Pixel",typeof(GameObject));
					
				Tile parentTile;
				SubTile subTile = m_Level.GetSubtileFromBoard(m_SelectedSubTile.row,m_SelectedSubTile.col,out parentTile);
				
				if (m_SubTileGizmoGO == null)
				{
					m_SubTileGizmoGO = Instantiate(obj,subTile.gameObject.transform.position,Quaternion.Euler(new Vector3(90.0f,00.0f,0.0f))) as GameObject;
				}
				
				m_SubTileGizmoGO.transform.position = subTile.transform.position;
			}
			else
			{
				if (m_SubTileGizmoGO != null)
					GameObject.DestroyImmediate(m_SubTileGizmoGO);
				m_SelectedSubTile = null;
				
				bool gizmoApplied = false;
				
				if (m_SelectedGizmoGO != null)
				{
					SelectionGizmo.GizmoType gizmoType = CheckGizmoClicked();
					if ( gizmoType != SelectionGizmo.GizmoType.eCount )
					{
						gizmoApplied = true;
						DoGizmoOperation(gizmoType);
						EditorUtility.SetDirty(m_Level.m_CurrentBoard);
						Repaint();
					}
				}
				if (!gizmoApplied)
					SelectTile();
			}
		}
			
		if (m_TileSelected != null)
		{
			GameObject tileSelected = m_Level.GameTiles[m_TileSelected.row * m_Level.m_CurrentBoard.m_ColCount + m_TileSelected.col].m_Tile.gameObject;
			Vector3 position = tileSelected.transform.position;
			
			if (m_SelectedGizmoGO == null)
			{
				Object obj = Resources.Load("Prefabs/SelectionGizmo",typeof(GameObject));
				m_SelectedGizmoGO = Instantiate(obj,position,Quaternion.Euler(new Vector3(90.0f,00.0f,0.0f))) as GameObject;
				SelectionGizmo gizmo = m_SelectedGizmoGO.GetComponent<SelectionGizmo>();
				if (gizmo != null)
				{
					gizmo.UpdateGizmoColliders();
				}
			}
			else
			{
				m_SelectedGizmoGO.transform.position = position;
			}
			
			Handles.color = Color.red;
			
			HandleUtility.Repaint();
			HandleUtility.AddDefaultControl (controlID);
			
			
			position.y = 0.1f;
			Handles.SelectionFrame(controlID,position,Quaternion.Euler(new Vector3(90.0f,0.0f,0.0f)),1.5f);
				
			if (Event.current.type == EventType.MouseDrag) 
			{
			}
		}
		
	}
	
	private void SelectTile() {
		
		GameObject go = HandleUtility.PickGameObject(Event.current.mousePosition,false);
		if (go != null && !CheckParentIsGizmo(go))
		{
			m_TileSelected = null;
			
			while(go)
			{
				Tile tile = go.GetComponent<Tile>();
				if (tile != null)
				{
					m_TileSelected = tile.Index;
					Repaint();
					break;
				}
				else
				{
					if (go.transform.parent)
						go = go.transform.parent.gameObject;
					else
						go = null;
				}
			}
		}
		else
			m_TileSelected = null;
		
		if (m_TileSelected == null)
		{
			if (m_SelectedGizmoGO != null)
				GameObject.DestroyImmediate(m_SelectedGizmoGO);
			Selection.activeObject = null;
		}
	
	}
	
	private void SelectSubTile()
	{
		GameObject go = HandleUtility.PickGameObject(Event.current.mousePosition,false);

		if (go != null)
		{
			m_SelectedSubTile = null;
			
			while(go)
			{
				SubTile subTile = go.GetComponent<SubTile>();
				if (subTile != null)
				{
					GameObject parent = go.transform.parent.gameObject;
					Tile tile = parent.GetComponent<Tile>();
					
					m_SelectedSubTile = m_Level.ChooseSubTileIndexFromPosition(go.transform.position,parent.transform.position,tile.Index);
					Repaint();
					break;
				}
				else
				{
					if (go.transform.parent)
						go = go.transform.parent.gameObject;
					else
						go = null;
				}
			}
		}
		else
		{
			m_SelectedSubTile = null;
		}
		
		if (m_SelectedSubTile == null)
		{
			GameObject sceneCamObj = GameObject.Find( "SceneCamera" );
			if ( sceneCamObj != null )
			{
				//Ray newRay = sceneCamObj.camera.ScreenPointToRay(Event.current.mousePosition);
				Ray newRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
				m_SelectedSubTile = m_Level.ChooseSubTileIndexFromRayCollide(newRay);
				Repaint();
			}
			
			
			if (m_SelectedSubTile == null)
			{
				Selection.activeObject = null;
			}
		}
	}
	
	private SelectionGizmo.GizmoType CheckGizmoClicked()
	{
		GameObject go = HandleUtility.PickGameObject(Event.current.mousePosition,false);
		if (go != null && CheckParentIsGizmo(go))
		{
			SelectionGizmo gizmo = m_SelectedGizmoGO.GetComponent<SelectionGizmo>();
			if (gizmo != null)
			{
				GameObject goToCheck = GetParentUnderOnelevelDownFromGizmo(go);
				for (int i = 0 ; i < (int)SelectionGizmo.GizmoType.eCount ; i++)
				{
					if (goToCheck == gizmo.GizmoColliders[i].gameObject)
						return (SelectionGizmo.GizmoType)i;
				}
			}
		}
		
		return SelectionGizmo.GizmoType.eCount;
	}
	
	private bool CheckParentIsGizmo(GameObject go)
	{
		Transform trans = go.transform;
		
		while(trans)
		{
			if (trans.gameObject == m_SelectedGizmoGO)
				return true;
			trans = trans.parent;
		}
		return false;
	}
	
	private GameObject GetParentUnderOnelevelDownFromGizmo(GameObject go)
	{
		Transform trans = go.transform;
		
		while(trans)
		{
			if (trans.parent != null && trans.parent.gameObject == m_SelectedGizmoGO)
				return trans.gameObject;
			trans = trans.parent;
		}
		return null;
	}
	
	
	public override void OnInspectorGUI () {
		
		if (EditorApplication.isPlaying)
		{
			return;
		}
		
		Board selectedBoard = m_Level.m_CurrentBoard;
		
		if (selectedBoard == null)
		{
			Selection.activeObject = null;
			return;
		}
		
		//string path = "Assets/Resources/Boards/";
		
		string newName = EditorGUILayout.TextField("BoardName",selectedBoard.name);
		
		if (newName != selectedBoard.name)
			AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(selectedBoard),newName);
		
		//RenameAsset (pathName : String, newName : String) : String 
		
		int newRowCount = EditorGUILayout.IntField("Row Count",selectedBoard.m_RowCount);
		int newColCount = EditorGUILayout.IntField("Col Count",selectedBoard.m_ColCount);
		
		if (newRowCount > 0 && newRowCount != selectedBoard.m_RowCount)
		{
			UpdateBoard(selectedBoard,newRowCount,newColCount);
			EditorUtility.SetDirty(selectedBoard);
		}
		
		if (newColCount > 0 && newColCount != selectedBoard.m_ColCount)
		{
			UpdateBoard(selectedBoard,newRowCount,newColCount);
			EditorUtility.SetDirty(selectedBoard);
		}
		
		if (m_SelectedSubTile != null)
		{
			Tile outParentTile;
			SubTile boardSubtile = m_Level.GetSubtileFromBoard(m_SelectedSubTile.row,m_SelectedSubTile.col,out outParentTile);
			
			bool isAlreadySpawnPoint = (m_Level.m_CurrentBoard.m_SpawnSubtile.row == m_SelectedSubTile.row && m_Level.m_CurrentBoard.m_SpawnSubtile.col == m_SelectedSubTile.col);
			bool spawnPoint = EditorGUILayout.Toggle("Spawn point",isAlreadySpawnPoint);
			if (spawnPoint)
			{
				m_Level.m_CurrentBoard.m_SpawnSubtile = new Index(m_SelectedSubTile.row,m_SelectedSubTile.col);
				EditorUtility.SetDirty(selectedBoard);
			}
			
			if (m_AllItemsNames != null)
			{
				m_CurrItem = EditorGUILayout.Popup("Item To Add",m_CurrItem,m_AllItemsNames);
				
				if (GUILayout.Button("Add Item"))
				{
					Tile parentTile;
					m_Level.GetSubtileFromBoard(m_SelectedSubTile.row,m_SelectedSubTile.col,out parentTile);
					
					int subTileRow = (int)m_SelectedSubTile.row%Tile.SubTileRowCount;
					int subTileCol = (int)m_SelectedSubTile.col%Tile.SubTileColCount;
					
					
					AddItemToSubtile(m_ItemTypes[m_CurrItem] as GameObject, parentTile.Index,new Index(subTileRow,subTileCol));
					EditorUtility.SetDirty(selectedBoard);
				}
				
				EditorGUILayout.Space();
				EditorGUILayout.Space();
				
				Board.TileData boardTileData = m_Level.GetBoardTile(outParentTile.Index.row,outParentTile.Index.col);
				ArrayList removeItems = null;
				
				//string[] subTileItems = boardTileData.m_Items;
				
				EditorGUILayout.BeginVertical();

				if (boardTileData.m_Items != null)
				{
					for (int i = 0; i < boardTileData.m_Items.Length ; i++)
					{
						//if (boardSubtile.
							EditorGUILayout.LabelField(boardTileData.m_Items[i].m_Item.name);
							if (GUILayout.Button("Remove Item"))
							{
								if (removeItems == null)
									removeItems = new ArrayList();
								removeItems.Add(i);
							}
					}
				}
				
				EditorGUILayout.EndVertical();
				
				if (removeItems != null && removeItems.Count > 0)
				{
					int a = 0;

					int newCount = boardTileData.m_Items.Length - removeItems.Count;
					
					Board.SubTileItemData[] oldData = boardTileData.m_Items;
					
					boardTileData.m_Items = new Board.SubTileItemData[newCount];
					
					for (int i = 0 ; i < newCount ; i++)
					{
						bool toBeRemoved = false;
						
						for (int p = 0 ; p < removeItems.Count ; p++)
						{
							if (((int)removeItems[p]) == i)
								toBeRemoved = true;
						}
						
						if (!toBeRemoved)
						{
							Board.SubTileItemData newData = new Board.SubTileItemData();
							boardTileData.m_Items[a++] = newData;
							newData.m_Item = oldData[i].m_Item;
							newData.m_LocalIndex = oldData[i].m_LocalIndex;
						}
					}
					
					foreach (SubTile subtile in outParentTile.GameSubTiles)
					{
						if (subtile.SubTileItems != null)
						{
							foreach (SubTileItem currItem in subtile.SubTileItems)
							{
								GameObject.DestroyImmediate(currItem.gameObject);
							}
							subtile.SubTileItems.Clear();
						}
					}
					
					outParentTile.CreateItems(boardTileData);
					EditorUtility.SetDirty(selectedBoard);
				}
				
			}
		}
		else if (m_TileSelected != null)
		{
			int index = m_TileSelected.row * m_Level.m_CurrentBoard.m_ColCount + m_TileSelected.col;
			Tile tileSelectedBoard = selectedBoard.m_Tiles[index].m_Tile;
			//Tile tileSelectedLevel = m_Level.GameTiles[index];
			
			int selectedInd = FindIndexOfItem(tileSelectedBoard.transform.name,m_TileTypeItems);
			int newItem = selectedInd;
			
			if (selectedInd >= 0)
				newItem = EditorGUILayout.Popup("Tile",selectedInd,(string[]) m_TileTypeItems.ToArray( typeof( string ) ));
			
			if (newItem != selectedInd)
			{
				selectedBoard.m_Tiles[index].m_Tile = m_AllTileTypes[newItem] as Tile;
								
				if (selectedBoard.m_Tiles[index].m_Tile.name == "TileNone")
				{
					selectedBoard.m_Tiles[index].m_TileType = Tile.Type.eNone;
				}
				else
				{
					if (selectedBoard.m_Tiles[index].m_TileType == Tile.Type.eNone)
						selectedBoard.m_Tiles[index].m_TileType = Tile.Type.eDefault;
				}
																	
				Vector3 position = m_Level.GameTiles[index].m_Tile.transform.position;
				Transform parent = m_Level.GameTiles[index].m_Tile.transform.parent;
				GameObject inst = Instantiate(selectedBoard.m_Tiles[index].m_Tile.gameObject) as GameObject;
				GameObject.DestroyImmediate(m_Level.GameTiles[index].m_Tile.gameObject);
				
				m_Level.GameTiles[index].m_Tile = inst.GetComponent<Tile>();
				
				m_Level.GameTiles[index].m_Tile.TileData = selectedBoard.m_Tiles[index];
				
				m_Level.GameTiles[index].m_Tile.transform.position = position;
				m_Level.GameTiles[index].m_Tile.transform.parent = parent;
				m_Level.GameTiles[index].m_Tile.Orientation = selectedBoard.m_Tiles[index].m_Orientation;
				m_Level.GameTiles[index].m_Tile.TileType = selectedBoard.m_Tiles[index].m_TileType;
				
				m_Level.GameTiles[index].m_Tile.UpdateTileBasedOnOrientation(true);
				
				EditorUtility.SetDirty(selectedBoard);
			}
			
			Tile.Type newType = (Tile.Type)EditorGUILayout.EnumPopup("Tile Type",selectedBoard.m_Tiles[index].m_TileType);
			if (newType != selectedBoard.m_Tiles[index].m_TileType)
			{
				if (selectedBoard.m_Tiles[index].m_Tile.name == "TileNone")
				{
					selectedBoard.m_Tiles[index].m_TileType = Tile.Type.eNone;
					m_Level.GameTiles[index].m_Tile.TileType = Tile.Type.eNone;
				}
				else
				{
					if (newType != Tile.Type.eNone)
					{
						selectedBoard.m_Tiles[index].m_TileType = newType;
						m_Level.GameTiles[index].m_Tile.TileType = newType;
					}
				}
			}
			
			Tile.OrientationType newOrient = (Tile.OrientationType)EditorGUILayout.EnumPopup("Orientation",selectedBoard.m_Tiles[index].m_Orientation);
			if (newOrient != m_Level.GameTiles[index].m_Tile.Orientation)
			{
				selectedBoard.m_Tiles[index].m_Orientation = newOrient;
				m_Level.GameTiles[index].m_Tile.Orientation = newOrient;
				m_Level.GameTiles[index].m_Tile.UpdateTileBasedOnOrientation(true);
				
				EditorUtility.SetDirty(selectedBoard);
			}
			
		}
	}
	
	private int FindIndexOfItem(string item,ArrayList items)
	{
		for (int i = 0; i < items.Count ; i++)
		{
			string currItem = items[i] as string;
			if(item == currItem)
				return i;
		}
		return -1;
	}
	
	private void UpdateBoard(Board board, int newRowCount, int newColCount)
	{
		if (newRowCount * newColCount <= 0)
		{
			board.m_RowCount = newRowCount;
			board.m_ColCount = newColCount;
			return;
		}
			
		if (board.m_Tiles == null || board.m_Tiles.Length == 0)
		{
			board.m_Tiles = new Board.TileData[newRowCount * newColCount];
			
			for (int i = 0 ; i < newRowCount ; i++)
			{
				for (int j = 0 ; j < newColCount ; j++)
				{
					board.m_Tiles[i * newColCount + j] = new Board.TileData();
					
					board.m_Tiles[i * newColCount + j].m_Tile = m_AllTileTypes[0] as Tile;
					board.m_Tiles[i * newColCount + j].m_Tile.Index.row = i;
					board.m_Tiles[i * newColCount + j].m_Tile.Index.col = j;
					board.m_Tiles[i * newColCount + j].m_TileType = Tile.Type.eNone;
					board.m_Tiles[i * newColCount + j].m_Orientation = Tile.OrientationType.eOrientation_0;
					
					board.m_Tiles[i * newColCount + j].m_Tile.TileData = board.m_Tiles[i * newColCount + j];
				}
			}
			
			if (newRowCount * newColCount == 0)
			{
				board.m_RowCount = newRowCount;
				board.m_ColCount = newColCount;
				return;
			}
		}
		else
		{
			Board.TileData[] newTiles = new Board.TileData[newRowCount * newColCount];

			for (int i = 0 ; i < newRowCount ; i++)
			{
				for (int j = 0 ; j < newColCount ; j++)
				{
					if ( (i >= board.m_RowCount) || (j >= board.m_ColCount) )
					{
						newTiles[i * newColCount + j] = new Board.TileData();
						
						newTiles[i * newColCount + j].m_Tile = m_AllTileTypes[0] as Tile;
						newTiles[i * newColCount + j].m_TileType = Tile.Type.eNone;
						newTiles[i * newColCount + j].m_Orientation = Tile.OrientationType.eOrientation_0;
						
						newTiles[i * newColCount + j].m_Tile.TileData = newTiles[i * newColCount + j];
					}
					else
					{
						if (board.m_Tiles[i * board.m_ColCount + j] == null)
						{
							newTiles[i * newColCount + j] = new Board.TileData();
							
							newTiles[i * newColCount + j].m_Tile = m_AllTileTypes[0] as Tile;
							newTiles[i * newColCount + j].m_TileType = Tile.Type.eNone;
							newTiles[i * newColCount + j].m_Orientation = Tile.OrientationType.eOrientation_0;
							
							newTiles[i * newColCount + j].m_Tile.TileData = newTiles[i * newColCount + j];
						}
						else
						{
							newTiles[i * newColCount + j] = board.m_Tiles[i * board.m_ColCount + j];
							newTiles[i * newColCount + j].m_Tile.TileData = newTiles[i * newColCount + j];
						}
					}
				}
			}
			
			board.m_Tiles = newTiles;
		}
		
		board.m_RowCount = newRowCount;
		board.m_ColCount = newColCount;
		
		LevelBoard level = m_LevelGO.GetComponent<LevelBoard>();
		level.DestroyBoard();
		level.LoadCurrentBoard();
	}
	
	public void SwapTiles(Index first, Index second)
	{
		SwapTiles(m_Level.m_CurrentBoard.m_Tiles,first,second);
		SwapTiles(m_Level.GameTiles,first,second);
	}
		
	public void SwapTiles(Board.TileData[] data, Index firstInd, Index secondInd)
	{		
		Board.TileData first = data[firstInd.row * m_Level.m_CurrentBoard.m_ColCount + firstInd.col];
		Board.TileData second = data[secondInd.row * m_Level.m_CurrentBoard.m_ColCount + secondInd.col];
		
		data[firstInd.row * m_Level.m_CurrentBoard.m_ColCount + firstInd.col] = second;
		data[secondInd.row * m_Level.m_CurrentBoard.m_ColCount + secondInd.col] = first;
		
		Index temp = second.m_Tile.Index;
		second.m_Tile.Index = first.m_Tile.Index;
		first.m_Tile.Index = temp;
		
		Vector3 tempPos = first.m_Tile.transform.position;
		first.m_Tile.transform.position = second.m_Tile.transform.position;
		second.m_Tile.transform.position = tempPos;
	}
	
	private void DoGizmoOperation(SelectionGizmo.GizmoType type)
	{
		if (m_TileSelected == null)
			return;
		
		switch(type)
		{
		case SelectionGizmo.GizmoType.eLeft:
			Index currIndex = m_TileSelected;
			Board.TileData selectedTileData = m_Level.GetGameTile(currIndex.row,currIndex.col);
			Board.TileData tileToSwap = m_Level.GetGameTile(currIndex.row,currIndex.col - 1);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapTiles(tileToSwap.m_Tile.Index,selectedTileData.m_Tile.Index);
					m_SelectedGizmoGO.transform.position = selectedTileData.m_Tile.transform.position;
					m_TileSelected = selectedTileData.m_Tile.Index;
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eRight:
			currIndex = m_TileSelected;
			selectedTileData = m_Level.GetGameTile(currIndex.row,currIndex.col);
			tileToSwap = m_Level.GetGameTile(currIndex.row,currIndex.col + 1);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapTiles(tileToSwap.m_Tile.Index,selectedTileData.m_Tile.Index);
					m_SelectedGizmoGO.transform.position = selectedTileData.m_Tile.transform.position;
					m_TileSelected = selectedTileData.m_Tile.Index;
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eUp:
			currIndex = m_TileSelected;
			selectedTileData = m_Level.GetGameTile(currIndex.row,currIndex.col);
			tileToSwap = m_Level.GetGameTile(currIndex.row-1,currIndex.col);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapTiles(tileToSwap.m_Tile.Index,selectedTileData.m_Tile.Index);
					m_SelectedGizmoGO.transform.position = selectedTileData.m_Tile.transform.position;
					m_TileSelected = selectedTileData.m_Tile.Index;
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eDown:
			currIndex = m_TileSelected;
			selectedTileData = m_Level.GetGameTile(currIndex.row,currIndex.col);
			tileToSwap = m_Level.GetGameTile(currIndex.row+1,currIndex.col);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapTiles(tileToSwap.m_Tile.Index,selectedTileData.m_Tile.Index);
					m_SelectedGizmoGO.transform.position = selectedTileData.m_Tile.transform.position;
					m_TileSelected = selectedTileData.m_Tile.Index;
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eRotateClockWise:
			int index = m_TileSelected.row * m_Level.m_CurrentBoard.m_ColCount + m_TileSelected.col;
			
			m_Level.m_CurrentBoard.m_Tiles[index].m_Orientation = RotateTileClockwise(m_Level.m_CurrentBoard.m_Tiles[index].m_Orientation);
			m_Level.GameTiles[index].m_Tile.Orientation = m_Level.m_CurrentBoard.m_Tiles[index].m_Orientation;
			m_Level.GameTiles[index].m_Tile.UpdateTileBasedOnOrientation(true);
			break;
			
		case SelectionGizmo.GizmoType.eRotateAntiClockWise:
			index = m_TileSelected.row * m_Level.m_CurrentBoard.m_ColCount + m_TileSelected.col;
			
			m_Level.m_CurrentBoard.m_Tiles[index].m_Orientation = RotateTileAntiClockwise(m_Level.m_CurrentBoard.m_Tiles[index].m_Orientation);
			m_Level.GameTiles[index].m_Tile.Orientation = m_Level.m_CurrentBoard.m_Tiles[index].m_Orientation;
			m_Level.GameTiles[index].m_Tile.UpdateTileBasedOnOrientation(true);
			break;
			
		default:
			break;
		}
	}
	
	private Tile.OrientationType RotateTileClockwise(Tile.OrientationType orientation)
	{
		orientation += 1;
		
		if (orientation > Tile.OrientationType.eOrientation_270)
			orientation = Tile.OrientationType.eOrientation_0;
		
		return orientation;
	}
	
	private Tile.OrientationType RotateTileAntiClockwise(Tile.OrientationType orientation)
	{
		orientation -= 1;
		
		if (orientation < 0)
			orientation = Tile.OrientationType.eOrientation_270;
		
		return orientation;
	}
	
	public void AddItemToSubtile(GameObject templateItem, Index tileIndex, Index subTileIndex)	{
		
		if (templateItem != null)
		{
			SubTileItem item = templateItem.GetComponent<SubTileItem>();
			if (item != null)
			{
				Board.TileData tileDataBoard = m_Level.GetBoardTile(tileIndex.row,tileIndex.col);
				Board.TileData tileDataGame = m_Level.GetGameTile(tileIndex.row,tileIndex.col);
				
				Board.SubTileItemData[] itemDataAll = tileDataBoard.m_Items;
				
				tileDataBoard.m_Items = null;

				int prv_size = 0;
				if (itemDataAll != null)
					prv_size = itemDataAll.Length;
				
				tileDataBoard.m_Items = new Board.SubTileItemData[prv_size + 1];
				
				if (itemDataAll != null)
				{
					for (int i = 0 ; i < itemDataAll.Length ; i++)
					{
						tileDataBoard.m_Items[i] = itemDataAll[i];
					}
				}
				
				tileDataBoard.m_Items[itemDataAll.Length] = new Board.SubTileItemData();
				tileDataBoard.m_Items[itemDataAll.Length].m_Item = item;
				tileDataBoard.m_Items[itemDataAll.Length].m_LocalIndex = new Index(subTileIndex.row,subTileIndex.col);
				
				foreach (SubTile subtile in tileDataGame.m_Tile.GameSubTiles)
				{
					if (subtile.SubTileItems != null)
					{
						foreach (SubTileItem currItem in subtile.SubTileItems)
						{
							GameObject.DestroyImmediate(currItem.gameObject);
						}
						subtile.SubTileItems.Clear();
					}
				}
				
				tileDataGame.m_Tile.CreateItems(tileDataBoard);
			}
		}
	}
}
