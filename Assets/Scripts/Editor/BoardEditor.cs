﻿using UnityEngine;
using UnityEditor;
using System.Collections;


[CustomEditor(typeof(Board))] 
public class BoardEditor : Editor {
	
	
	// Add a menu item named "Do Something" to MyMenu in the menu bar.
	[MenuItem ("Assets/Create/Board")]
	static void CreateBoard () {
		
		string path = "Assets/Resources/Boards";
		
		ScriptableObjectUtility.CreateAsset<Board>(path);
	}
	
	[MenuItem ("Assets/Create/ObjectDataTrain")]
	static void CreateObjectDataTrain () {
		
		string path = "Assets/Resources/ObjectData";
		
		ScriptableObjectUtility.CreateAsset<ObjectDataTrain>(path);
	}
	
	[MenuItem ("Assets/Create/Objectives/ObjectivePlayerReachedTarget")]
	static void CreateObjectivePlayerReachedTarget () {
		
		string path = "Assets/Resources/Objectives";
		
		ScriptableObjectUtility.CreateAsset<ObjectivePlayerReachedTarget>(path);
	}
	
	[MenuItem ("Assets/Create/Objectives/ObjectiveTrainReachedTarget")]
	static void CreateObjectiveTrainReachedTarget () {
		
		string path = "Assets/Resources/Objectives";
		
		ScriptableObjectUtility.CreateAsset<ObjectiveTrainReachedTarget>(path);
	}
	
	private GameObject m_Level;
	
	
	void OnEnable () {
		
		if (EditorApplication.isPlaying)
			return;
		
		UnityEngine.Object levelPrefab = Resources.Load("Prefabs/LevelBoard");
				
		if (m_Level == null)
			m_Level = Instantiate(levelPrefab,Vector3.zero,Quaternion.identity) as GameObject;
		
		if (m_Level == null)
			Debug.LogError("LevelBoard not created");
		
		Board selectedBoard = Selection.activeObject as Board;
		
		if (selectedBoard == null)
		{
			Selection.activeObject = null;
		}
		
		
		LevelBoard level = m_Level.GetComponent<LevelBoard>();
		level.m_CurrentBoard = selectedBoard;
		Selection.activeObject = m_Level;
	}
	
}
