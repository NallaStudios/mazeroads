using UnityEngine;
using System;
using System.Collections;

public class Flag
{
	private long			m_Flag = 0;
	
	
	public Flag() {
		m_Flag = 0;
	}
	
	public Flag(long flag) {
		m_Flag = flag;
	}
	
	public void SetFlag(long flag) {
		m_Flag = m_Flag | flag;
	}
	
	public void ClearFlag(long flag) {
		m_Flag = m_Flag & ~flag;
	}
	
	public bool CheckFlag(long flag) {
		return (m_Flag & flag) == flag;
	}
	
	public void Reset() {
		m_Flag = 0;
	}
}
