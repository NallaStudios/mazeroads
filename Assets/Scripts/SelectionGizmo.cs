﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class GizmoObject
{
	public Collider m_GizmoLeft;
	public Collider m_GizmoRight;
	public Collider m_GizmoUp;
	public Collider m_GizmoDown;
	public Collider m_GizmoRotateClockWise;
	public Collider m_GizmoRotateAntiClockWise;
};

public class SelectionGizmo : MonoBehaviour {
	
	public enum GizmoType
	{
		eLeft = 0,
		eRight,
		eUp,
		eDown,
		eRotateClockWise,
		eRotateAntiClockWise,
		
		eCount
	}
	
	public GizmoObject m_Gizmos;
	
	private Collider[] m_GizmoColliders;
	
	public Collider[] GizmoColliders
	{
		get
		{
			return m_GizmoColliders;
		}
	}

	// Use this for initialization
	void Start () {
		UpdateGizmoColliders();	
	}
	
	public void UpdateGizmoColliders() {
		
		m_GizmoColliders = new Collider[(int)GizmoType.eCount];
		
		m_GizmoColliders[(int)GizmoType.eLeft] = m_Gizmos.m_GizmoLeft;
		m_GizmoColliders[(int)GizmoType.eRight] = m_Gizmos.m_GizmoRight;
		m_GizmoColliders[(int)GizmoType.eUp] = m_Gizmos.m_GizmoUp;
		m_GizmoColliders[(int)GizmoType.eDown] = m_Gizmos.m_GizmoDown;
		m_GizmoColliders[(int)GizmoType.eRotateClockWise] = m_Gizmos.m_GizmoRotateClockWise;
		m_GizmoColliders[(int)GizmoType.eRotateAntiClockWise] = m_Gizmos.m_GizmoRotateAntiClockWise;
	
	}
	
	public GizmoType CheckGizmoClicked(Ray ray)
	{
		for (int i = 0 ; i < (int)GizmoType.eCount ; i++)
		{
			RaycastHit hit;
			if (m_GizmoColliders[i].Raycast(ray,out hit,1000.0f))
				return (GizmoType)i;
		}
				
		return GizmoType.eCount;
	}

}
