﻿using UnityEngine;
using System.Collections;

// copied from net, working so didn't care to read the code.... parag
public class Bresenham {
  
    public delegate bool PlotFunction(int x, int y);

    public static void Line(int x0, int y0, int x1, int y1, PlotFunction plot)
    {
        bool steep = Mathf.Abs(y1 - y0) > Mathf.Abs(x1 - x0);
        if (steep) { Utilities.Swap<int>(ref x0, ref y0); Utilities.Swap<int>(ref x1, ref y1); }
        if (x0 > x1) { Utilities.Swap<int>(ref x0, ref x1); Utilities.Swap<int>(ref y0, ref y1); }
        int dX = (x1 - x0), dY = Mathf.Abs(y1 - y0), err = (dX / 2), ystep = (y0 < y1 ? 1 : -1), y = y0;

        for (int x = x0; x <= x1; ++x)
        {
            if (!(steep ? plot(y, x) : plot(x, y))) return;
            err = err - dY;
            if (err < 0) { y += ystep;  err += dX; }
        }
    }
}
