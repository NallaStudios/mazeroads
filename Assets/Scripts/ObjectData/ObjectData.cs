﻿using UnityEngine;
using System.Collections;

public abstract class ObjectData : ScriptableObject {

	// Use this for initialization
	protected abstract void Start ();
}
