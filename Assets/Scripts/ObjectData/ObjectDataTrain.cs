﻿using UnityEngine;
using System.Collections;

public class ObjectDataTrain : ObjectData {
	
	public Vector3 m_StartOrientation;
	
	private Index m_EndIndex;
	private Train m_Train;
	
	public Train Train
	{
		set
		{
			m_Train = value;
		}
		get
		{
			return m_Train;
		}
	}
		
	public Index EndIndex
	{
		get
		{
			return m_EndIndex;
		}
		set
		{
			m_EndIndex = value;
		}
	}

	protected override void Start () 
	{

	}
}
