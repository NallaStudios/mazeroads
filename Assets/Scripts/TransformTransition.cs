﻿using UnityEngine;
using System.Collections;

public class TransformTransition {
	
	public enum TransitionType
	{
		eTranslate,
		eRotate,
	}
	
	private bool m_Transitioning;
	
	private TransitionType m_TranstionType;
	
	private Transform m_Source;// source whice needs to transform
	private Vector3 m_Target; // for translate represents position, for rotate represents euler angles, for scale x,y,z scale
	
	
	private Vector3 m_Speed; //x used for speed and rotation angle, x,y,z used for scale
	
	private Vector3 m_Direction;
	
	
	public TransformTransition()
	{
		m_Transitioning = false;
	}
	
	public void StartTransitioning(TransitionType type, Transform source, Vector3 target, Vector3 speed) {
		
		m_TranstionType = type;
		m_Source = source;
		m_Speed = speed;
		
		m_Target = target;
		
		m_Transitioning = true;
		
		switch(m_TranstionType) {
		case TransitionType.eTranslate:
			m_Direction = m_Target - source.localPosition;
			m_Direction.Normalize();
			break;
		case TransitionType.eRotate:
			Vector3 currDir = m_Source.localRotation * Vector3.forward;
			Quaternion targetOrt = Quaternion.Euler(target);
			Vector3 targetDir = targetOrt * Vector3.forward;
			m_Direction = Vector3.Cross(currDir,targetDir);
			m_Direction.Normalize();
			break;
		}
	}
	
	public void Update(float dt) {
		
		if (m_Transitioning)
		{
			switch(m_TranstionType) {
			case TransitionType.eTranslate:
				Vector3 distVec = m_Target - m_Source.localPosition;
				float distTranslate = m_Speed.x * dt;
				float EPSILON = 0.001f;
				if (distVec.magnitude <= (distTranslate + EPSILON) || Vector3.Dot(distVec,m_Direction) <= 0.0f)
				{
					m_Source.localPosition = m_Target;
					m_Transitioning = false;
				}
				else
				{
					m_Source.localPosition = m_Source.localPosition + m_Direction * distTranslate;
				}
				break;
				
			case TransitionType.eRotate:
				Quaternion rot = Quaternion.AngleAxis(m_Speed.x * dt,m_Direction);
				m_Source.localRotation = rot * m_Source.localRotation;
				
				Vector3 currDir = m_Source.localRotation * Vector3.forward;
				
				Quaternion targetOrt = Quaternion.Euler(m_Target);
				Vector3 targetDir = targetOrt * Vector3.forward;
				Vector3 newDir = Vector3.Cross(currDir,targetDir);
				newDir.Normalize();
				
				if (Vector3.Dot(newDir,m_Direction) <= 0.0f)
				{
					m_Source.localEulerAngles = m_Target;
					m_Transitioning = false;
				}
				
				//Vector3 sourceDir = 
				break;
			}
		}
	}
	
	public bool TransitionCompleted()	{
		
		return !m_Transitioning;
	}
}
