﻿using UnityEngine;
using System.Collections;

public class AStar {

	public delegate float Heuristic(Index start, Index end);
	public delegate float MovementCost(Index start, Index end);
	public delegate Index[] WalkableNeighbours(Index current);
	public delegate void SetAStarData(Index index, System.Object data);
	public delegate System.Object GetAStarData(Index index);
	
	private class Node
	{
		public Node(Node parent, Index ind, float heuristic, float movement, SetAStarData setData)
		{
			parentNode = parent;
			index = ind;
			heuristicCost = heuristic;
			movementCost = movement;
			totalCost = heuristic + movement;
			open = true;
			setData(index,this);
		}
		
		public bool open;
		public Index index;
		public float heuristicCost;
		public float movementCost;
		public float totalCost;
		public Node parentNode;
	}
	
	
	public static Index[] FindPath(Index start, Index end, Heuristic heuristic, MovementCost movementCost, WalkableNeighbours neighbours, SetAStarData setData, GetAStarData getData)
	{
		ArrayList openList = new ArrayList();
		ArrayList closedList = new ArrayList();
		
		openList.Add(new Node(null,start,0.0f,0.0f,setData));
		
		Node endNode = null;
		
		//Node lowestHCostNode = openList[0] as Node;
				
		while(openList.Count > 0)
		{
			Node lowestCostNode = FindNodeWithLowestTotalCost(openList);
			Index[] currNeighbours = neighbours(lowestCostNode.index);
			
			AddToClosedList(openList,closedList,lowestCostNode);
			
			//lastLowCostNode = lowestCostNode;
			
			if (lowestCostNode.index.row == end.row && lowestCostNode.index.col == end.col)
			{
				endNode = lowestCostNode;
				break;
			}
			
			foreach (Index neighbour in currNeighbours)
			{
				Node neighbourNode = getData(neighbour) as Node;
				if (neighbourNode == null || neighbourNode.open)
				{
					float hCostLocal = heuristic(neighbour,end);
					float mCostLocal = movementCost(lowestCostNode.index,neighbour);
					float mCost = mCostLocal + lowestCostNode.movementCost;
					float totalCost = hCostLocal + mCost;
					
					if (neighbourNode == null)
					{
						neighbourNode = new Node(lowestCostNode,neighbour,hCostLocal,mCost,setData);
						openList.Add(neighbourNode);
					}
					else
					{
						if (neighbourNode.movementCost > mCost )
						{
							neighbourNode.parentNode = lowestCostNode;
							neighbourNode.heuristicCost = hCostLocal;
							neighbourNode.movementCost = mCost;
							neighbourNode.totalCost = totalCost;
						}
					}
					
					/*if ( neighbourNode.heuristicCost < lowestHCostNode.heuristicCost)
					{
						lowestHCostNode = neighbourNode;
					}*/
				}
			}
		}
		
		ArrayList path = new ArrayList();
		
		if (endNode != null)
		{
			while (endNode != null)
			{
				path.Add(endNode.index);
				
				endNode = endNode.parentNode;
			}
			
			path.Reverse();
			
			return (Index[])path.ToArray(typeof(Index));
		}
		
		/*while (lowestHCostNode != null)
		{
			path.Add(lowestHCostNode.index);
			
			lowestHCostNode = lowestHCostNode.parentNode;
		}
		
		path.Reverse();*/
		
		return (Index[])path.ToArray(typeof(Index));

	}
	
	private static void AddToClosedList(ArrayList openList, ArrayList closedList, Node node)
	{
		openList.Remove(node);
		closedList.Add(node);
		node.open = false;
	}
	
	private static Node FindNodeWithLowestTotalCost(ArrayList nodes)
	{
		Node lowest = null;
		float lowestCost = float.MaxValue;
		foreach (Node node in nodes)
		{
			if (node.totalCost < lowestCost)
			{
				lowest = node;
				lowestCost = node.totalCost;
			}
		}
		
		return lowest;
	}
}
