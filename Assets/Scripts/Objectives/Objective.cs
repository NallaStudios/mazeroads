﻿using UnityEngine;
using System.Collections;

public abstract class Objective : ScriptableObject {
	
	public abstract bool ObjectiveCompleted ();
}
