﻿using UnityEngine;
using System.Collections;

public class ObjectivePlayerReachedTarget : Objective {

	public override bool ObjectiveCompleted ()
	{
		Player player = Game.Instance.Player;
		return player.PlayerReachedTarget;
	}
}
