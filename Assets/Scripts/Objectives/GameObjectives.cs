﻿using UnityEngine;
using System.Collections;

public class GameObjectives : MonoBehaviour {
	
	public Objective[] m_AllObjectives;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public bool AllObjectivesCompleted()
	{
		foreach(Objective objective in m_AllObjectives)
		{
			if (!objective.ObjectiveCompleted())
				return false;
		}
		
		return true;
	}
}
