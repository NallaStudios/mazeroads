﻿using UnityEngine;
using System.Collections;

public class ObjectiveTrainReachedTarget: Objective {
	
	public ObjectDataTrain m_TrainData;

	public override bool ObjectiveCompleted ()
	{
		if (m_TrainData.Train != null)
		{
			return m_TrainData.Train.FinalDestinationReached;
		}
		return false;
	}
}
