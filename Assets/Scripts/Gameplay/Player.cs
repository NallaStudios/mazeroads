using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerProperties
{
	public float								m_WalkTurnAngle = 45.0f;
}

public class Player : Character
{
	public enum PlayerStates
	{
		ePS_Idle = 0,		// Idle
		ePS_Walk,			// Walk
		
		ePS_Count,
	}
	
	public PlayerProperties						m_Properties;
	private BufferedInput						m_BufferedInput;
	private StateMachine						m_PlayerStateMachine = new StateMachine();
	private State[]								m_AllStates;
	
	private LevelBoard							m_CurrentLevelBoard;
	
	private Index[]								m_CurrentPath;
	private Index								m_CurrSubtile;
	private Index								m_TargetSubtile;
	private int									m_CurrIndexInPath;
	
	private ArrayList							m_PlayerInventory;
	private Vector3								m_UseItemMousePos;
	private Index								m_UseItemEdgeSubtile1;
	private Index								m_UseItemEdgeSubtile2;
	private GameObject							m_ConnectionGO;
	private bool								m_PlayerReachedTarget = false;
	
	private ArrayList							m_AllConnectionGO;
	
	public LevelBoard CurrentLevelBoard
	{
		get
		{
			return m_CurrentLevelBoard;
		}
		set
		{
			m_CurrentLevelBoard = value;
		}
	}
	
	public Index CurrSubtile
	{
		get
		{
			return m_CurrSubtile;
		}
		set
		{
			m_CurrSubtile = value;
		}
	}
	
	public ArrayList Inventory
	{
		get
		{
			return m_PlayerInventory;
		}
	}
	
	public bool PlayerReachedTarget
	{
		get
		{
			return m_PlayerReachedTarget;
		}
	}
	
	public void SetToBoard()
	{
		Tile parentTile;
		SubTile subtile = m_CurrentLevelBoard.GetSubtileFromBoard(m_CurrentLevelBoard.m_CurrentBoard.m_SpawnSubtile.row,m_CurrentLevelBoard.m_CurrentBoard.m_SpawnSubtile.col,out parentTile);
		Vector3 pos = subtile.gameObject.transform.position;
		transform.position = pos;
		
		m_CurrSubtile = m_CurrentLevelBoard.m_CurrentBoard.m_SpawnSubtile;
		m_CurrIndexInPath = 0;
		
		m_PlayerInventory = new ArrayList();
		
		m_CurrentLevelBoard.m_CanSelectTile += CanSelectTile;
		
		m_PlayerReachedTarget = false;
		
		ClearAllConnectionGO();
	}
	
	protected void Awake()
	{
		m_BufferedInput = GetComponent<BufferedInput>();
		if( m_BufferedInput == null )
		{
			Debug.LogError("Buffered input is not attached to game object '" + gameObject.name + "'");
			Debug.Break();
		}
		m_AllStates = new State[(int)PlayerStates.ePS_Count];
		InitializeAllStates();
		
		//CurrentGravityScale = 0.15f;
	}
	
	// Use this for initialization
	protected override void Start () 
	{
		base.Start();
		
		m_PlayerReachedTarget = false;
		
		/*if (CurrentLevelBoard != null)
		{
			SetToBoard();
		}*/
	}
	
	// Update is called once per frame
	protected override void Update ()
	{
		base.Update();
		
		m_PlayerStateMachine.Update(Time.deltaTime);
	}
	
	private void MoveToNextSubTile(float dt)
	{
		CurrentMoveSpeed = 2.0f;
	}
	
	// Called before called Attacked if returns false Attacked will get called even if weapon
	// Collides with this actor
	public override bool CanAttack(Actor attacker)
	{
		if( attacker.tag == "Enemy" )
		{
			return true;
		}
		return false;
	}

	// Called when any types weapon collides with this, returns true if it is successful attack
	// returns false if attack is blocked
	public override bool Attacked(Actor attacker, GameObject bodyPartGotHit , float damage)
	{

		return true;
	}
	
	// Called when attack is blocked
	public override void CurrentAttackBlocked(Actor defender)
	{
	}
	
	// Called when an animation event happens
	public override void OnAnimEvent(AnimEventData data)
	{
		base.OnAnimEvent(data);
		if( data.GetType() == typeof(PlayerAnimEventData) )
		{

		}
		else
		{
			Debug.Log("AnimEventData not supported in Player");
		}
	}
	
	public override void OnItemReceived(SubTileItem item)
	{
		if (item.IsType(typeof(SubTileItemFinishPoint).ToString()))
		{
			m_PlayerReachedTarget = true;
			item.Enabled = false;
			item.gameObject.SetActive(false);
		}
		else if (item.IsType(typeof(SubTileItemBridge).ToString()))
		{
			m_PlayerInventory.Add(item);
			item.Enabled = false;
			item.gameObject.SetActive(false);
		}
	}
	
	// -------------------------------------------------------------------------
	// Start State functions
	
	public void InitializeAllStates()
	{
		m_PlayerStateMachine.AddNewTransition(CheckMoveStates);
		
		
		m_AllStates[(int)PlayerStates.ePS_Idle] = m_PlayerStateMachine.AddNewState("Idle",BeginIdleState,UpdateIdleState,EndIdleState);
		m_AllStates[(int)PlayerStates.ePS_Walk] = m_PlayerStateMachine.AddNewState("Walk",BeginMoveState,UpdateMoveState,EndMoveState);
	}
	
	public State CheckMoveStates(State currState)
	{	
		if (Input.GetMouseButtonUp(1))
		{
			return m_AllStates[(int)PlayerStates.ePS_Walk];
		}
		
		return m_AllStates[(int)PlayerStates.ePS_Idle];
	}
	
	// -----------------
	// Start Idle State
	public void BeginIdleState(State prvState, State newState)
	{
		Ray newRay = new Ray(transform.position + new Vector3(0.0f,20.0f,0.0f),new Vector3(0.0f,-1.0f,0.0f));
		m_CurrSubtile = m_CurrentLevelBoard.ChooseSubTileIndexFromRayCollide(newRay);	
	}
	
	public void UpdateIdleState(State currState, float deltaTime)
	{
	}
	
	public void EndIdleState(State endingState, State newState)
	{

	}
	
	// End Idle State
	// -----------------
	
	// -----------------
	// Start Walk, Run State
	public void BeginMoveState(State prvState, State newState)
	{
		ChangePath();
			
		m_PlayerStateMachine.CanChangeState = false;
	}
	
	private bool CanMoveToTileType(SubTile.SubTileType type)
	{
		return type == SubTile.SubTileType.eRoad;
	}
	
	private bool ChangePath()
	{
		Ray newRay = new Ray(transform.position + new Vector3(0.0f,20.0f,0.0f),new Vector3(0.0f,-1.0f,0.0f));
		m_CurrSubtile = m_CurrentLevelBoard.ChooseSubTileIndexFromRayCollide(newRay);	
		
		Ray ray = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
		
		Index pos = CurrentLevelBoard.ChooseSubTileIndexFromRayCollide(ray);
		
		PathFinder newFinder = new PathFinder();
		
		m_CurrentPath = newFinder.FindPath(CurrentLevelBoard,m_CurrSubtile,pos,CanMoveToTileType);
		m_CurrIndexInPath = 0;
		
		m_TargetSubtile = GetNextSubtile();
		
		if (m_TargetSubtile == null) 
			return false;
		
		SetCharacterFlag(Character.CharacterFlag.eCF_ResetMoveSpeedAfterUse);
		
		Tile parentTile;
		SubTile nxtSubtile = CurrentLevelBoard.GetSubtileFromBoard(m_TargetSubtile.row,m_TargetSubtile.col,out parentTile);
		
		Vector3 targetPos = nxtSubtile.transform.position;
		Vector3 targetDir = targetPos - transform.position;
		
		targetDir.Normalize();
		
		MoveDirection = targetDir;
		
		return true;
	}
	
	public void UpdateMoveState(State currState, float deltaTime)
	{
		if (Input.GetMouseButtonUp(1))
		{
			ChangePath();
		}
		else if (m_CurrentPath != null && m_TargetSubtile != null)
		{
			Tile parentTile;
			SubTile nxtSubtile = CurrentLevelBoard.GetSubtileFromBoard(m_TargetSubtile.row,m_TargetSubtile.col,out parentTile);
			
			Vector3 targetPos = nxtSubtile.transform.position;
			Vector3 targetDir = targetPos - transform.position;
			
			targetDir.Normalize();
			
			float dist = targetDir.magnitude;
			
			float EPSILON = 0.125f;
			if (dist < EPSILON || Vector3.Dot(targetDir,MoveDirection) <= 0.0f)
			{
				//transform.position = targetPos;
				m_CurrSubtile = m_TargetSubtile;
				m_CurrIndexInPath++;
				m_TargetSubtile = GetNextSubtile();
				
				if (m_TargetSubtile == null)
				{
					m_PlayerStateMachine.CanChangeState = true;
					CurrentMoveSpeed = 0.0f;
				}
				else
				{
					nxtSubtile = CurrentLevelBoard.GetSubtileFromBoard(m_TargetSubtile.row,m_TargetSubtile.col,out parentTile);
			
					targetPos = nxtSubtile.transform.position;
					targetDir = targetPos - transform.position;
				
					targetDir.Normalize();
				
					MoveDirection = targetDir;
				}
			}
			else
			{
				CurrentMoveSpeed = 5.0f;
			}
			
			Rotate(targetDir);
		}
		else
		{
			m_PlayerStateMachine.CanChangeState = true;
		}
		
		if (m_CurrentLevelBoard.m_SelectedTile)
			m_PlayerStateMachine.CanChangeState = true;
		//Rotate(m_Properties.m_WalkTurnAngle, deltaTime);
	}
	
	public void EndMoveState(State endingState, State newState)
	{
		ClearCharacterFlag(Character.CharacterFlag.eCF_ResetMoveSpeedAfterUse);
	}
	
	private Index GetNextSubtile()
	{
		if ( (m_CurrIndexInPath + 1) < m_CurrentPath.Length )
		{
			Index nextIndex = m_CurrentPath[m_CurrIndexInPath + 1];
			
			/*if ( Mathf.Abs(nextIndex.row-m_CurrSubtile.row) > 1 || Mathf.Abs(nextIndex.col-m_CurrSubtile.col) > 1)
			{
				Debug.LogError("Next index can be only one tile next to currentSubtile");
			}*/
			return nextIndex;
		}
		return null;
	}
	
	// End Walk, Run State
	// -----------------
	
	
	// -------------------------------------------------------------------------
	
	private bool Rotate(Vector3 targetDir)
	{
		Vector3 currDir = transform.forward;
		
		targetDir.y = 0.0f;
		currDir.y = 0.0f;
		
		targetDir.Normalize();
		currDir.Normalize();
		
		
		transform.forward = Vector3.Slerp(currDir,targetDir,0.2f);
		
		return (Vector3.Dot(currDir,transform.forward) >= 0.95f);
	}
	
	public bool TryUseItem(SubTileItem item, bool use)
	{
		Vector3 mousePos = Input.mousePosition;
		
		if (m_UseItemMousePos != mousePos)
		{
			m_UseItemMousePos = mousePos;
			
			Index neighbour;
			m_CurrentLevelBoard.GetTwoSubtileBasedOn2DPositionOnEdge(mousePos, out m_UseItemEdgeSubtile1,out neighbour);
			
			Index diff = new Index(m_UseItemEdgeSubtile1.row - neighbour.row,m_UseItemEdgeSubtile1.col - neighbour.col);
			
			if ( Mathf.Abs(diff.row) > 1 || Mathf.Abs(diff.col) > 1 )
			{
				Debug.LogError("Difference can't be more than 1..");
			}
			
			if (item.IsType(typeof(SubTileItemBridge).ToString() ))
			{
				SubTileItemBridge bridge = item as SubTileItemBridge;
				
				if ( bridge != null && bridge.m_ConnectionPrefab != null)
				{
					if (m_ConnectionGO == null)
						m_ConnectionGO = Instantiate(bridge.m_ConnectionPrefab.gameObject) as GameObject;
					SubTileConnection connection = m_ConnectionGO.GetComponent<SubTileConnection>();
					if (m_UseItemEdgeSubtile2 == null)
						m_UseItemEdgeSubtile2 = new Index();
					
					m_UseItemEdgeSubtile2.row = m_UseItemEdgeSubtile1.row + diff.row * (connection.m_ConnectionGap + 1);
					m_UseItemEdgeSubtile2.col = m_UseItemEdgeSubtile1.col + diff.col * (connection.m_ConnectionGap + 1);
				}
			}
		}
		
		SubTile subTile1 = null,subTile2 = null;
		
		if (m_ConnectionGO != null && m_UseItemEdgeSubtile1.row < m_CurrentLevelBoard.BoardSubtileRowCount && m_UseItemEdgeSubtile1.col < m_CurrentLevelBoard.BoardSubtileColCount
			&& m_UseItemEdgeSubtile2.row < m_CurrentLevelBoard.BoardSubtileRowCount && m_UseItemEdgeSubtile2.col < m_CurrentLevelBoard.BoardSubtileColCount 
			&& m_UseItemEdgeSubtile1.row >= 0 && m_UseItemEdgeSubtile1.col >= 0 && m_UseItemEdgeSubtile2.row >= 0 && m_UseItemEdgeSubtile2.col >= 0)
		{
			Tile parentTile1;
			subTile1 = m_CurrentLevelBoard.GetSubtileFromBoard(m_UseItemEdgeSubtile1.row,m_UseItemEdgeSubtile1.col, out parentTile1);
			
			Tile parentTile2;
			subTile2 = m_CurrentLevelBoard.GetSubtileFromBoard(m_UseItemEdgeSubtile2.row,m_UseItemEdgeSubtile2.col, out parentTile2);
			
			if (subTile1 != null && subTile2 != null)
			{
				Vector3 distVec = subTile1.transform.position - subTile2.transform.position;
				Vector3 centerPos = (subTile1.transform.position + subTile2.transform.position) * 0.5f;
				
				Vector3 dir = distVec.normalized;
				
				dir.y = 0.0f;
				dir.Normalize();
				
				Quaternion rotation = Quaternion.FromToRotation(Vector3.forward,dir);
				
				m_ConnectionGO.transform.rotation = rotation;
				m_ConnectionGO.transform.position = centerPos;
				
				m_ConnectionGO.transform.parent = m_CurrentLevelBoard.transform;
			}
		}
		
		if (use)
		{
			if (subTile1 != null && subTile2 != null && subTile1.m_SubTileType != SubTile.SubTileType.eNone && subTile2.m_SubTileType != SubTile.SubTileType.eNone)
			{
				SubTileConnection connection = m_ConnectionGO.GetComponent<SubTileConnection>();
				subTile1.AllConnections.Add(connection);
				subTile2.AllConnections.Add(connection);
				
				connection.Connection1 = m_UseItemEdgeSubtile1;
				connection.Connection2 = m_UseItemEdgeSubtile2;
				
				Index[] indices = GetIndicesFromSubTile1ToSubTile2(m_UseItemEdgeSubtile1,m_UseItemEdgeSubtile2,connection.m_DisableSubtileCount);
				
				if (indices != null && indices.Length > 0)
				{
					for (int i = 0 ; i < indices.Length ; i++)
					{
						LevelBoard.StaticSubTileData staticData = m_CurrentLevelBoard.GetStaticSubTileData(indices[i]);
						if (staticData != null)
						{
							staticData.m_SubTileEnabled = false;
						}
					}
				}
				
				Inventory.Remove(item);
				
				//m_ConnectionGO.transform.parent = m_CurrentLevelBoard.transform;
				if (m_AllConnectionGO == null)
					m_AllConnectionGO = new ArrayList();
				m_AllConnectionGO.Add(m_ConnectionGO);
				
				m_ConnectionGO = null;
				
				m_UseItemEdgeSubtile1 = null;
				m_UseItemEdgeSubtile2 = null;
				
				
				return true;
			}
			else
			{
				Game.Destroy(m_ConnectionGO);
				m_ConnectionGO = null;
			}
		}
			
		return false;
	}
	
	public void ClearAllConnectionGO()
	{
		if (m_AllConnectionGO == null)
			return;
		
		foreach(GameObject go in m_AllConnectionGO)
		{
			GameObject.Destroy(go);
		}
	}
	
	private Index[] GetIndicesFromSubTile1ToSubTile2(Index subtile1, Index subtile2, int maxCount)
	{
		ArrayList indices = new ArrayList();
		if (subtile1.row == subtile2.row)
		{
			if ( subtile2.col > subtile1.col )
			{
				int end = subtile1.col + maxCount + 1;
				
				if (end > m_CurrentLevelBoard.BoardSubtileColCount )
				{
					end = m_CurrentLevelBoard.BoardSubtileColCount;
				}
				
				int p = 1;
				for (int i = subtile1.col + 1 ; i < end ; i++)
				{
					indices.Add(new Index(subtile1.row,i));
					indices.Add(new Index(subtile1.row,subtile2.col - p));
					p++;
				}
			}
			else
			{
				int end = subtile2.col + maxCount + 1;
				
				if (end > m_CurrentLevelBoard.BoardSubtileColCount )
				{
					end = m_CurrentLevelBoard.BoardSubtileColCount;
				}
				
				int p = 1;
				for (int i = subtile2.col + 1 ; i < end ; i++)
				{
					indices.Add(new Index(subtile2.row,i));
					indices.Add(new Index(subtile2.row,subtile1.col - p));
					p++;
				}
			}
		}
		else if (subtile1.col == subtile2.col)
		{
			if ( subtile2.row > subtile1.row )
			{
				int end = subtile1.row + maxCount + 1;
				
				if (end > m_CurrentLevelBoard.BoardSubtileRowCount )
				{
					end = m_CurrentLevelBoard.BoardSubtileRowCount;
				}
				
				int p = 1;
				for (int i = subtile1.row + 1 ; i < end ; i++)
				{
					indices.Add(new Index(i,subtile1.col));
					indices.Add(new Index(subtile2.row-p,subtile1.col));
					p++;
				}
			}
			else
			{
				int end = subtile2.row + maxCount + 1;
				
				if (end > m_CurrentLevelBoard.BoardSubtileRowCount )
				{
					end = m_CurrentLevelBoard.BoardSubtileRowCount;
				}
				
				int p = 1;
				for (int i = subtile2.row + 1 ; i < end ; i++)
				{
					indices.Add(new Index(i,subtile2.col));
					indices.Add(new Index(subtile1.row-p,subtile2.col));
					p++;
				}
			}
		}
		
		return (Index[])indices.ToArray(typeof(Index));
	}
	
	public bool CanSelectTile(Tile tile)
	{
		if (tile.HasConnection())
		{
			return false;
		}
		
		Tile playerTile;
		m_CurrentLevelBoard.GetSubtileFromBoard(m_CurrSubtile.row,m_CurrSubtile.col,out playerTile);
		
		if (tile.Index.Equals(playerTile.Index))
			return false;
		return true;
	}
}
