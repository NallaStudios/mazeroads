using UnityEngine;
using System.Collections;

public class GameInputMessageGenerator : MessageGenerator 
{
	private static GameInputMessageGenerator m_MessageGenerator = null;
	
	protected void Awake()
	{
		if( m_MessageGenerator != null )
		{
			Debug.LogError("Only one instance of 'GameInputMessageGenerator' can be created");
			Debug.Break();
		}
		m_MessageGenerator = this;
	}
		
	// Use this for initialization
	protected override void Start () 
	{
	
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		float vertValue = Input.GetAxisRaw("Vertical");
		float horzValue = Input.GetAxisRaw("Horizontal");
		
		//Debug.Log("Left = " + vertValue + ", Forward = " + horzValue ); 
		
		const float epsilon = 0.0001f;
		
		if( Mathf.Abs(vertValue) > epsilon )
		{
			SendMessageToListeners("MoveForward",vertValue);
			
			//Debug.Log("Send mssg 1");
		}
		if( Mathf.Abs(horzValue) > epsilon )
		{
			//Debug.Log("Send mssg 2");
			if( horzValue < 0.0f )
			{
				SendMessageToListeners("TurnLeft");
			}
			else
			{
				SendMessageToListeners("TurnRight");
			}
		}
		
		if (Input.GetKey(KeyCode.LeftShift) ) 
		{ 
			SendMessageToListeners("Walk");
		}
	}
	
	public override MessageGenerator Get()
	{
		return m_MessageGenerator;
	}
}
