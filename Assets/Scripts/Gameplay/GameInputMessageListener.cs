using UnityEngine;
using System.Collections;

public class GameInputMessageListener : MessageListener 
{
	protected override void Awake()
	{
		base.Awake();
	}
	// Use this for initialization
	protected override void Start () 
	{
	
	}
	
	protected override void Update()
	{
	}
	
	void MoveForward(float fwdValue)
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("MoveForward",0.15f,fwdValue);
			//Debug.Log("Received mssg 1");
		}
	}
	
	void MoveRight(float rightValue)
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("MoveRight",0.15f,rightValue);
			//Debug.Log("Received mssg 2");
		}
	}
	
	void FaceDirectionX(float fwdValue)
	{
		if( m_TargetBufferedInput != null )
		{
			//m_TargetBufferedInput.AddInput("FaceFront",0.15f,fwdValue);
			//Debug.Log("Received mssg 1");
		}
	}
	
	void FaceDirectionY(float rightValue)
	{
		if( m_TargetBufferedInput != null )
		{
			//m_TargetBufferedInput.AddInput("FaceRight",0.15f,rightValue);
			//Debug.Log("Received mssg 2");
		}
	}
	
	void TurnLeft()
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("TurnLeft",0.15f);
			//Debug.Log("Received mssg 2");
		}
	}
	
	void TurnRight()
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("TurnRight",0.15f);
			//Debug.Log("Received mssg 2");
		}
	}
	
	void Walk()
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("Walk",0.15f);
			//Debug.Log("Received message Walk");
		}
	}
	
	void Attack()
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("Attack",0.5f);
			//Debug.Log("Received message Walk");
		}
	}
	
	void Jump()
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("Jump",0.3f);
			//Debug.Log("Received message Walk");
		}
	}
	
	void Roll()
	{
		if( m_TargetBufferedInput != null )
		{
			m_TargetBufferedInput.AddInput("Roll",0.25f);
			//Debug.Log("Received message Walk");
		}
	}
}
