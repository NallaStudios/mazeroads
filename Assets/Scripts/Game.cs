﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
	
	private static Game m_Instance;
	
	
	public Board[] m_Board;
	public Camera m_Camera;
	public GameObject m_GizmoPrefabRef;
	public GameObject m_Pixel;// for debugging
	
	
	private GameObject m_LevelGO;
	private GameObject m_PlayerGO;
	
	private bool m_LevelFinished;
	private float m_LevelFinishedTime;
	private int m_CurrLevel;
	private SubTileItem m_ItemToUse;
	private bool m_GUIEventHappened;
	
	public static Game Instance
	{
		get
		{
			return m_Instance;
		}
	}
	
	public GameObject GizmoPrefabRef
	{
		get
		{
			return m_GizmoPrefabRef;
		}
	}
	
	public Player Player
	{
		get
		{
			Player player = m_PlayerGO.GetComponent<Player>();
			return player;
		}
	}

	// Use this for initialization
	void Start () {
		
		m_Instance = this;
		
		UnityEngine.Object levelPrefab = Resources.Load("Prefabs/LevelBoard");
				
		if (m_LevelGO == null)
			m_LevelGO = Instantiate(levelPrefab,Vector3.zero,Quaternion.identity) as GameObject;
	
		ChangeLevel();
		
		m_LevelFinished = false;
		m_CurrLevel = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (m_ItemToUse != null && !m_GUIEventHappened)
		{
			Player player = m_PlayerGO.GetComponent<Player>();
			
			if (Input.GetMouseButtonUp(0))
			{
				player.TryUseItem(m_ItemToUse,true);
				{
					m_ItemToUse = null;
				}
				
			}
			else
			{
				player.TryUseItem(m_ItemToUse,false);
			}
		}
		
		LevelBoard level = m_LevelGO.GetComponent<LevelBoard>();
			
		if (level.GameObjectives != null && level.GameObjectives.AllObjectivesCompleted())
		{
			OnLevelEnded(level);
			level.DestroyObjectives();
		}
		
		if (m_GUIEventHappened )
			m_GUIEventHappened = false;
	}
	
	public void OnLevelEnded(LevelBoard level)
	{
		m_LevelFinishedTime = 3.0f;
		m_LevelFinished = true;
		m_CurrLevel++;
	}
	
	void ChangeLevel()
	{
		LevelBoard level = m_LevelGO.GetComponent<LevelBoard>();
		level.m_CurrentBoard = m_Board[m_CurrLevel];
		
		level.DestroyBoard();
		level.LoadCurrentBoard();
		
		if (m_PlayerGO != null)
		{
			Player playerPrv = m_PlayerGO.GetComponent<Player>();
			playerPrv.ClearAllConnectionGO();
			GameObject.Destroy(m_PlayerGO);
		}
		
		UnityEngine.Object playerPrefab = Resources.Load("Prefabs/Player");
		m_PlayerGO = Instantiate(playerPrefab,Vector3.zero,Quaternion.identity) as GameObject;
		Player player = m_PlayerGO.GetComponent<Player>();
		player.CurrentLevelBoard = level;
		
		m_LevelFinished = false;
		
		player.SetToBoard();
		
		
		Vector3 playerPos = m_PlayerGO.transform.position;
		playerPos.y = m_Camera.transform.position.y;
		m_Camera.transform.position = playerPos;
		
		player.CurrentLevelBoard.StartClampCamera();
	}
	
	void OnGUI () {
		
		float width = Screen.width;
		float height = Screen.height;
		
		if (m_LevelFinished)
		{
			m_LevelFinishedTime -= Time.deltaTime;
			if (m_LevelFinishedTime <= 0.0f )
			{
				if (m_CurrLevel < m_Board.Length)
				{
					ChangeLevel();
				}
				else
				{
				}
			}
			else
			{
				float halfWidth = width * 0.5f;
				float halfHeight = height * 0.5f;
				if (m_CurrLevel < m_Board.Length)
				{
					GUI.Label (new Rect (halfWidth-200, halfHeight, 500, 40), "You Completed the Level!");
				}
				else
				{
					GUI.Label (new Rect (halfWidth-200, halfHeight, 500, 40), "You Completed the Game!");
				}
				
			}
		}
		else
		{
			if (m_PlayerGO != null)
			{
				Player player = m_PlayerGO.GetComponent<Player>();
			
				float size = 64;
				float gap = 8;
				float startHeight = height - 128;
				if (player.Inventory != null && player.Inventory.Count > 0)
				{
					foreach ( SubTileItem item in player.Inventory)
					{
						if (item.m_ItemUIIconTexture != null)
						{
							if (GUI.Button (new Rect (width - 128,startHeight , size,size),item.m_ItemUIIconTexture))
							{
								m_ItemToUse = item;
								m_GUIEventHappened = true;
							}
							
							startHeight -= gap + size;
						}
					}
				}
			}
		}
	}
}
