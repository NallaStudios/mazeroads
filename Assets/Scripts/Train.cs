﻿using UnityEngine;
using System.Collections;

public class Train : Actor {
	
	public TrainBoggy m_StartBoggy;
	
	public float m_TrainSpeed;
	
	private Index m_StartPoint;
	
	private static readonly float TimerMax = 1.0f;
	private float m_Timer = TimerMax;
	private Index[] m_Path = null;
	private LevelBoard m_LevelBoard = null;
	private ObjectDataTrain m_TrainData;
	
#if UNITY_EDITOR
	private bool m_StartChoosen;
	private Index m_StartPos;
	private ArrayList m_LineGO;
#endif
	
	public bool FinalDestinationReached
	{
		get
		{
			return m_StartBoggy.FinalDestinationReached;
		}
	}
	
			
	protected override void Start () 
	{
#if UNITY_EDITOR
		m_StartChoosen = false;
#endif
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
#if UNITY_EDITOR
		//DebugDrawBresenhamLine();
#endif
		
		if (m_TrainData != null && m_TrainData.EndIndex != null && m_Path == null)
		{
			m_Timer -= Time.deltaTime;
			
			if (m_Timer < 0.0f)
			{
				FindPath();
				m_Timer = TimerMax;
			}
		}
	}
	
	private void FindPath()
	{
		PathFinder finder = new PathFinder();
		m_Path = finder.FindPath(m_LevelBoard,m_StartPoint,m_TrainData.EndIndex ,CanMoveToTileType);
		
		if (m_Path != null && m_Path.Length > 0)
			m_StartBoggy.Path = m_Path;
		else
			m_Path = null;
		
	}
			
	private bool CanMoveToTileType(SubTile.SubTileType type)
	{
		return type == SubTile.SubTileType.eRail;
	}
	
	public override void OnSpawned(LevelBoard board, Index index, ObjectData data)
	{
		m_LevelBoard = board;
		m_StartPoint = index;
		if (data != null && data.GetType() == typeof(ObjectDataTrain))
		{
			ObjectDataTrain trainData = data as ObjectDataTrain;
			transform.rotation = Quaternion.Euler(trainData.m_StartOrientation);
			m_TrainData = trainData;
			m_StartBoggy.CurrentBoard = m_LevelBoard;
			m_StartBoggy.Speed = m_TrainSpeed;
			
			trainData.Train = this;
#if UNITY_EDITOR
			//DebugPath();
#endif
		}
	}
	
	public override bool CanAttack(Actor attacker)
	{
		return false;
	}

	// Called when any types weapon collides with this, returns true if it is successful attack
	// returns false if attack is blocked
	public override bool Attacked(Actor attacker, GameObject bodyPartGotHit , float damage)
	{
		return false;
	}
	
	// Called when attack is blocked
	public override void CurrentAttackBlocked(Actor defender)
	{
	}
	
	// Called when animation event happens
	public override void OnAnimEvent(AnimEventData data)
	{
	}
	
	
	// Called when an item received
	public override void OnItemReceived(SubTileItem item)
	{
	}
	
#if UNITY_EDITOR
	private void DebugPath()
	{	
		DestroyAllLineGO();
		
		PathFinder newFinder = new PathFinder();
		Index[] path = newFinder.FindPath(m_LevelBoard,m_StartPoint,m_TrainData.EndIndex,CanMoveToTileType);
		
		foreach (Index index in path)
		{
			Plot(index.row,index.col);
		}
	}
	
	private void DebugDrawBresenhamLine()
	{
		if (Input.GetMouseButtonDown(1))
		{
			if (!m_StartChoosen)
			{
				Ray ray = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
				
				m_StartPos = m_LevelBoard.ChooseSubTileIndexFromRayCollide(ray);
				
				m_StartChoosen = true;
				
				DestroyAllLineGO();
				Plot(m_StartPos.row,m_StartPos.col);
			}
			else
			{
				Ray ray = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
				
				Index endPos = m_LevelBoard.ChooseSubTileIndexFromRayCollide(ray);
				
				m_StartChoosen = false;
				
				DestroyAllLineGO();
				//Bresenham.Line(m_StartPos.row,m_StartPos.col,endPos.row,endPos.col,Plot);
				
				PathFinder newFinder = new PathFinder();
				Index[] path = newFinder.FindPath(m_LevelBoard,m_StartPos,endPos,CanMoveToTileType);
				
				foreach (Index index in path)
				{
					Plot(index.row,index.col);
				}
			}
		}
	}
	
	private bool Plot(int x, int y)
	{
		Tile parentTile;
		SubTile subTile = m_LevelBoard.GetSubtileFromBoard(x,y,out parentTile);
		
		if (subTile != null)
		{
			GameObject go = subTile.gameObject;
			Vector3 pos = go.transform.position;
			pos.y += 0.1f;
			GameObject newGO = Instantiate(Game.Instance.m_Pixel,pos,Quaternion.Euler(90,0,0)) as GameObject;
			
			if (m_LineGO == null)
				m_LineGO = new ArrayList();
			m_LineGO.Add(newGO);
			
			return true;
		}
		
		return false;
	}
	
	private void DestroyAllLineGO()
	{
		if (m_LineGO != null)
		{
			foreach (GameObject go in m_LineGO)
			{
				GameObject.Destroy(go);
			}
			m_LineGO.Clear();
		}
	}
#endif
}
