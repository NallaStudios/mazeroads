﻿using UnityEngine;
using System.Collections;

public class SubTile : MonoBehaviour {
	
	public enum SubTileType
	{
		eNone,
		eRoad,
		eGrass,
		eWater,
		eStone,
		eSoil,
		eRail,
	}
	
	public SubTile.SubTileType m_SubTileType;
	
	public SubTile m_InternalConnection;
	
	private System.Object m_PathFinderData = null;
	private ArrayList m_SubTileItems;
	
	private ArrayList m_AllConnections = null;
	
	private Index m_LocalIndex;
	
	public System.Object PathFinderData	{
		
		get
		{
			return m_PathFinderData;
		}
		set
		{
			m_PathFinderData = value;
		}
	}
	
	public ArrayList AllConnections
	{
		get
		{
			return m_AllConnections;
		}
	}
	
	public Index LocalIndex
	{
		get
		{
			return m_LocalIndex;
		}
		set
		{
			m_LocalIndex = value;
		}
	}
	
	public void AddSubTileItem (SubTileItem item)	{
		
		if (m_SubTileItems == null)
			m_SubTileItems = new ArrayList();
		
		m_SubTileItems.Add(item);
	}
	
	public ArrayList SubTileItems	{
		
		get
		{
			return m_SubTileItems;
		}
	}
	
	public Index GetConnectionThroughNeighbour(Index selfIndex, Index neighbour, out SubTile.SubTileType connectionType)
	{
		connectionType = SubTile.SubTileType.eNone;
			
		Index diff = new Index(neighbour.row - selfIndex.row,neighbour.col - selfIndex.col);
		
		if (diff.row != 0)
			diff.row = diff.row/Mathf.Abs(diff.row);
		
		if (diff.col != 0)
			diff.col = diff.col/Mathf.Abs(diff.col);
		
		foreach (SubTileConnection connection in AllConnections )
		{
			Index neighbourDiff = null;
			bool connect1return = true;
			if (selfIndex.row == connection.Connection1.row && selfIndex.col == connection.Connection1.col)
			{
				neighbourDiff = new Index(connection.Connection2.row - selfIndex.row,connection.Connection2.col - selfIndex.col);
				connect1return = false;
			}
			else if (selfIndex.row == connection.Connection2.row && selfIndex.col == connection.Connection2.col)
			{
				neighbourDiff = new Index(connection.Connection1.row - selfIndex.row,connection.Connection1.col - selfIndex.col);
				connect1return = true;
			}
			
			if (neighbourDiff != null)
			{
				if (neighbourDiff.row != 0)
					neighbourDiff.row = neighbourDiff.row/Mathf.Abs(neighbourDiff.row);
				
				if (neighbourDiff.col != 0)
					neighbourDiff.col = neighbourDiff.col/Mathf.Abs(neighbourDiff.col);
				
				if ( diff.row == neighbourDiff.row && diff.col == neighbourDiff.col)
				{
					if (connect1return)
					{
						connectionType = connection.m_ConnectsSubTileType;
						return connection.Connection1;
					}
					else
					{
						connectionType = connection.m_ConnectsSubTileType;
						return connection.Connection2;
					}
				}
				neighbourDiff = null;
			}
		}
		
		return null;
	}
		
	// Use this for initialization
	void Start () {
		m_AllConnections = new ArrayList();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
