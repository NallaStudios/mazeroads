﻿using UnityEngine;
using System.Collections;

public class SubTileItemTrainTarget : SubTileItem {
	
	
	protected override void Start () {
		base.Start();
		
		if (m_Data != null && m_Data.GetType() == typeof(ObjectDataTrain))
		{
			ObjectDataTrain trainData = m_Data as ObjectDataTrain;
			
			Vector3 origin = transform.position + new Vector3(0.0f,20.0f,0.0f);
			Ray newRay = new Ray(origin,-Vector3.up);
			
			Index subtileInd = CurrentLevelBoard.ChooseSubTileIndexFromRayCollide(newRay);
			
			trainData.EndIndex = subtileInd;
		}
	}
	
	// Update is called once per frame
	protected override void Update () {
	}
}
