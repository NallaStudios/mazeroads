﻿using UnityEngine;
using System.Collections;

public class SubTileItem : ActorTriggerBase {
	
	private SubTile m_Parent;
	private LevelBoard m_LevelBoard;
	
	public Texture m_ItemUIIconTexture;
	
	public ObjectData m_Data;
	
	public SubTile Parent
	{
		get
		{
			return m_Parent;
		}
		set
		{
			m_Parent = value;
		}
	}
	
	public LevelBoard CurrentLevelBoard
	{
		get
		{
			return m_LevelBoard;
		}
		set
		{
			m_LevelBoard = value;
		}
	}
	
	public Bounds ItemBounds
	{
		get
		{
			return GetComponent<Collider>().bounds;
		}
	}
	
	// Use this for initialization
	protected override void Start () {
		m_DefaultEnabled = true;
		base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () {
	
	}
	
	protected virtual void OnTriggerEnter (Collider other) 
	{	
		if(Enabled == true )
		{
			Actor actor = other.gameObject.GetComponent<Actor>();
			if( actor != null )
			{	
				actor.OnItemReceived(this);
			}
		}
	}
}
