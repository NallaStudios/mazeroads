﻿using UnityEngine;
using System.Collections;

public class SubTileItemActorSpawner : SubTileItem {
	
	public Actor m_ActorToSpawn;
	
	private bool m_Spawned = false;
	
	protected override void Start () {
		base.Start();
		
		m_Spawned = false;
	}
	
	// Update is called once per frame
	protected override void Update () {
		
		if (!m_Spawned)
		{
			if (m_ActorToSpawn != null)
			{
				Vector3 origin = transform.position + new Vector3(0.0f,20.0f,0.0f);
				Ray newRay = new Ray(origin,-Vector3.up);
				
				Index subtileInd = CurrentLevelBoard.ChooseSubTileIndexFromRayCollide(newRay);
				
				GameObject actorObject = Instantiate(m_ActorToSpawn.gameObject,transform.position,Quaternion.identity) as GameObject;
				Actor actor = actorObject.GetComponent<Actor>();
				actor.OnSpawned(CurrentLevelBoard,subtileInd,m_Data);
				
				//ChooseSubTileIndexFromRayCollide(Ray ray)
			}
			m_Spawned = true;
			GameObject.Destroy(this.gameObject);
		}
	
	}
}
