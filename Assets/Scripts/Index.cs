﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Index
{
	public Index()
	{
		row = 0;
		col = 0;
	}
	
	public Index(int i, int j)
	{
		row = i;
		col = j;
	}
	public int row;
	public int col;
	
	public bool Equals(Index index)
	{
		return (row == index.row && col == index.col);
	}
	
	/*public static bool operator == (Index left, Index right) 
	{
		return (left.row == right.row && left.col == right.col)	;
	}
	
	public static bool operator != (Index left, Index right) 
	{
		return (left.row != right.row || left.col != right.col)	;
	}
	
	public override bool Equals (object obj)
	{
		return base.Equals (obj);
	}
	
	public override int GetHashCode ()
	{
		
		return base.GetHashCode();
	}*/
};
