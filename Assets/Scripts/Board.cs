﻿using UnityEngine;
using System.Collections;

public class Board : ScriptableObject {
	
	[System.Serializable]
	public class SubTileItemData {
		public Index m_LocalIndex;
		public SubTileItem m_Item;
	}
	
	[System.Serializable]
	public class TileData {
		public Tile m_Tile;
		public SubTileItemData[] m_Items;
		public Tile.Type m_TileType;
		public Tile.OrientationType m_Orientation;
	}
	
	public int m_RowCount;
	public int m_ColCount;
	
	public Index m_SpawnSubtile;
	
	public TileData[] m_Tiles;	
}
