﻿using UnityEngine;
using System.Collections;

public class PathFinder {
	
	public static readonly float CostMoveToTile = 10.0f;
	
	public delegate bool CanMoveToTileType(SubTile.SubTileType subTileType);
	
	private LevelBoard m_LevelBoard;
	private ArrayList m_Heuristic;
	
	private CanMoveToTileType m_CanMoveToTileType;
	
	public Index[] FindPath(LevelBoard levelBoard, Index start, Index end, CanMoveToTileType canMoveToTileType)
	{
		m_LevelBoard = levelBoard;
		SetPathFinderDataToAllSubTiles(null);
		
		m_CanMoveToTileType = canMoveToTileType;
		
		Index[] path = AStar.FindPath(start,end,Heuristic,MovementCost,WalkableNeighbours,SetPathFinderDataToSubTile,GetPathFinderDataFromSubTile);
		
		return path;
	}
	
	private float Heuristic(Index start, Index end)
	{
		if (m_Heuristic == null)
			m_Heuristic = new ArrayList();
		
		m_Heuristic.Clear();
		Bresenham.Line(start.row,start.col,end.row,end.col,Plot);
		
		float totalCost = CostMoveToTile * m_Heuristic.Count;
				
		return totalCost;
	}
	
	private float MovementCost(Index start, Index end)
	{
		//if (!IsNeighbourIndex(start,end))
		//	Debug.LogError("Some bug, none neighbour indices are asking for movement cost");
		
		return CostMoveToTile;
	}
	
	private Index GetConnection(Index current, Index neighbour, SubTile currentSubtile, out SubTile.SubTileType connectionType)
	{
		connectionType = SubTile.SubTileType.eNone;
		if (currentSubtile.AllConnections != null && currentSubtile.AllConnections.Count > 0)
		{
			Index connection = currentSubtile.GetConnectionThroughNeighbour(current,neighbour, out connectionType);
			if (connection != null)
			{
				//indices.Add(connection);
				return connection;
			}
		}
		return null;
	}
	
	private void AddNeighbourIndices(Tile currentParentTile, Index current,SubTile currentSubtile, Index neighbour, ref ArrayList indices)
	{
		Tile parentTile;
		SubTile subTile = m_LevelBoard.GetSubtileFromBoard(neighbour.row,neighbour.col,out parentTile);
		
		LevelBoard.StaticSubTileData staticData = m_LevelBoard.GetStaticSubTileData(neighbour);
		
		if (!staticData.m_SubTileEnabled)
			return;
		
		SubTile.SubTileType connectionType;
		
		Index connection = GetConnection(current,neighbour,currentSubtile, out connectionType);
		if (connection != null)
		{
			Tile connParent;
			SubTile connSubTile = m_LevelBoard.GetSubtileFromBoard(connection.row,connection.col,out connParent);
		
			if (connectionType != currentSubtile.m_SubTileType || connSubTile.m_SubTileType != connectionType)
				return;
			
			if (indices == null)
				indices = new ArrayList();
			indices.Add(connection);
		}
		else if (parentTile == currentParentTile && currentSubtile.m_InternalConnection != null )
		{
			if (indices == null)
				indices = new ArrayList();
			
			Index index = parentTile.GetSubTileBoardIndex(currentSubtile.m_InternalConnection);
			
			indices.Add(index);
		}
		else if (subTile != null && m_CanMoveToTileType(subTile.m_SubTileType))
		{
			if (indices == null)
				indices = new ArrayList();
			indices.Add(neighbour);
		}
	}
	
	private Index[] WalkableNeighbours(Index current)
	{
		ArrayList indices = null;//new ArrayList();
		
		Index neighbour1 = new Index(current.row + 1,current.col);
		Index neighbour2 = new Index(current.row - 1,current.col);
		Index neighbour3 = new Index(current.row,current.col + 1);
		Index neighbour4 = new Index(current.row,current.col - 1);
		
		Tile parentTile;
		SubTile currentSubtile = m_LevelBoard.GetSubtileFromBoard(current.row,current.col, out parentTile);
			
		AddNeighbourIndices(parentTile,current,currentSubtile,neighbour1,ref indices);
		AddNeighbourIndices(parentTile,current,currentSubtile,neighbour2,ref indices);
		AddNeighbourIndices(parentTile,current,currentSubtile,neighbour3,ref indices);
		AddNeighbourIndices(parentTile,current,currentSubtile,neighbour4,ref indices);
		
		return (Index[])indices.ToArray(typeof(Index));
	}
	
	private void SetPathFinderDataToSubTile(Index index, System.Object data)
	{
		Tile parentTile;
		SubTile subTile = m_LevelBoard.GetSubtileFromBoard(index.row,index.col,out parentTile);
		if (subTile == null)
			Debug.LogError("Bug: wrong subtitle index");
		subTile.PathFinderData = data;
	}
		
	private System.Object GetPathFinderDataFromSubTile(Index index)
	{
		Tile parentTile;
		SubTile subTile = m_LevelBoard.GetSubtileFromBoard(index.row,index.col,out parentTile);
		if (subTile == null)
			Debug.LogError("Bug: wrong subtitle index");
		return subTile.PathFinderData;
	}
	
	private void SetPathFinderDataToAllSubTiles(System.Object data)
	{
		for (int i = 0 ; i < m_LevelBoard.BoardSubtileRowCount ; i++)
		{
			for (int j = 0 ; j < m_LevelBoard.BoardSubtileColCount ; j++)
			{
				Tile parentTile;
				SubTile subTile = m_LevelBoard.GetSubtileFromBoard(i,j,out parentTile);
				if (subTile == null)
					Debug.LogError("Bug: wrong subtitle index");
				subTile.PathFinderData = data;
			}
		}
	}
	
	private bool Plot(int row, int col)
	{
		//Tile parentTile;
		//SubTile subTile = m_LevelBoard.GetSubtileFromBoard(row,col,out parentTile);
		
		m_Heuristic.Add(new Index(row,col));
		
		return true;
	}
	
	private bool IsNeighbourIndex(Index index1, Index index2)
	{
		if ( ((index1.row + 1) == index2.row || (index1.row - 1) == index2.row) && (index1.col == index2.col))
			return true;
		
		if ( ((index1.col + 1) == index2.col || (index1.col - 1) == index2.col) && (index1.row == index2.row) )
			return true;
		
		return false;
	}
	
}
