﻿using UnityEngine;
using System.Collections;

public class TrainBoggy : Actor {
	
	public TrainBoggy m_NextBoggy;
	public TrainBoggy m_PrvBoggy;
	
	public bool m_Engine;
	
	private Index[] m_Path;
	private LevelBoard m_CurrentBoard;
	
	private ArrayList m_PositionPath;
	
	private int m_CurrentPathIndex;
	private Vector3 m_MoveDir;
	
	private float m_DistFromNextBoggy;
	
	private float m_Speed;
	
	private bool m_FinalDestinationReached;
	
	public float Speed
	{
		get
		{
			return m_Speed;
		}
		set
		{
			m_Speed = value;
		}
	}
	
	public Index[] Path
	{
		get
		{
			return m_Path;
		}
		set
		{
			m_Path = value;
						
			if (Engine)
			{
				InitializePositionPath();
				m_CurrentPathIndex = 0;
				if (IsTargetReached())
					m_CurrentPathIndex++;
			}
			else
			{
				m_CurrentPathIndex = 0;
			}
			
			if (m_PrvBoggy != null)
				m_PrvBoggy.m_PositionPath = m_PositionPath;
			
			InitializeDistances();
			UpdateDirection();
			
			if (m_PrvBoggy != null)
				m_PrvBoggy.Path = m_Path;
		}
	}
	
	public bool Engine
	{
		get
		{
			return m_Engine;
		}
	}
	
	public LevelBoard CurrentBoard
	{
		get
		{
			return m_CurrentBoard;
		}
		set
		{
			m_CurrentBoard = value;
		}
	}
	
	public bool FinalDestinationReached
	{
		get
		{
			if (m_PrvBoggy != null)
			{
				return m_PrvBoggy.FinalDestinationReached;
			}
			return m_FinalDestinationReached;
		}
	}
	
		
	protected override void Start () 
	{

	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		
		if (m_Path != null && m_PositionPath != null)
		{
			if (Engine)
			{
				Vector3 position = transform.position;
				
				position += m_MoveDir * Speed * Time.deltaTime;
				
				transform.position = position;
				
				if (m_CurrentPathIndex < m_PositionPath.Count && IsTargetReached() )
				{
					m_CurrentPathIndex++;
					if (m_CurrentPathIndex < m_PositionPath.Count)
						UpdateDirection();
				}
			}
			
			if (m_PrvBoggy != null)
				m_PrvBoggy.UpdateFromParent();
			
			Vector3 forward = transform.forward;
			
			forward = Vector3.Slerp(forward,m_MoveDir,0.4f);
			
			transform.forward = forward;
		}
	}
	
	private void UpdateFromParent()
	{
		float distFromNext = DistFromNext();
				
		if (distFromNext > 0)
		{
			Vector3 position = transform.position;
		
			position += m_MoveDir * distFromNext;
			
			transform.position = position;
			
			if (m_CurrentPathIndex < m_PositionPath.Count && IsTargetReached() )
			{
				m_CurrentPathIndex++;
				if (m_CurrentPathIndex < m_PositionPath.Count)
				UpdateDirection();
			}
		}
	}
	
	private float DistFromNext()
	{
		int nextPrvTarget = m_NextBoggy.m_CurrentPathIndex - 1;
		Vector3 nextPos = m_NextBoggy.transform.position;
		
		float distFromNext = 0.0f;
		
		while(nextPrvTarget >= m_CurrentPathIndex)
		{
			Vector3 currPos = (Vector3)m_PositionPath[nextPrvTarget];
			Vector3 distFromNextVec = nextPos - currPos;
			distFromNextVec.y = 0.0f;
			
			distFromNext += distFromNextVec.magnitude;
			nextPos = currPos;
			
			nextPrvTarget--;
		}
		
		Vector3 distFromTarget = (Vector3)nextPos - transform.position;
		distFromTarget.y = 0.0f;
		
		distFromNext += distFromTarget.magnitude;
		
		float diff = distFromNext - m_DistFromNextBoggy;
		
		return diff;
	}
	
	private void InitializePositionPath()
	{
		Index prv = null;
		m_PositionPath = new ArrayList();
		
		foreach(Index index in Path)
		{
			Tile parentTile1;
			SubTile currentTarget = CurrentBoard.GetSubtileFromBoard(index.row,index.col,out parentTile1);
		
			if (prv != null)
			{
				Tile parentTile2;
				SubTile prvTarget = CurrentBoard.GetSubtileFromBoard(prv.row,prv.col,out parentTile2);
				
				if (IsDiagonalTiles(currentTarget,prvTarget))
				{
					m_PositionPath.Add((currentTarget.transform.position + prvTarget.transform.position) * 0.5f);
				}
			}

			m_PositionPath.Add(currentTarget.transform.position);
			prv = index;
		}
	}
	
	private bool IsTargetReached()
	{
		Vector3 distVec = (Vector3)m_PositionPath[m_CurrentPathIndex] - transform.position;
		distVec.y = 0.0f;
		
		float distance = distVec.magnitude;
		
		float DIST_EPSILON = 0.1f;
			
		if (distance < DIST_EPSILON || Vector3.Dot(distVec,m_MoveDir) < 0.0f )
			return true;
		
		return false;
	}
	
	private void InitializeDistances()
	{
		if (m_NextBoggy != null)
		{
			Vector3 distVec = m_NextBoggy.transform.position - transform.position;
			distVec.y = 0.0f;
			m_DistFromNextBoggy = distVec.magnitude;	
		}
	}
	
	private void UpdateDirection()
	{
		//if (m_CurrentPathIndex == 0)
		{
			m_MoveDir = (Vector3)m_PositionPath[m_CurrentPathIndex] - transform.position;
		}
		/*else
		{
			m_MoveDir = (Vector3)m_PositionPath[m_CurrentPathIndex] - (Vector3)m_PositionPath[m_CurrentPathIndex-1];
		}*/
		
		m_MoveDir.y = 0.0f;
		m_MoveDir.Normalize();
	}
	
	private bool IsDiagonalTiles(SubTile subtile1, SubTile subtile2)
	{
		Index index1 = subtile1.LocalIndex;
		Index index2 = subtile2.LocalIndex;
		
		if (index1.row == index2.row)
			return false;
		
		if (index1.col == index2.col)
			return false;
		
		return true;
	}
	
	public override bool CanAttack(Actor attacker)
	{
		return false;
	}

	// Called when any types weapon collides with this, returns true if it is successful attack
	// returns false if attack is blocked
	public override bool Attacked(Actor attacker, GameObject bodyPartGotHit , float damage)
	{
		return false;
	}
	
	// Called when attack is blocked
	public override void CurrentAttackBlocked(Actor defender)
	{
	}
	
	// Called when animation event happens
	public override void OnAnimEvent(AnimEventData data)
	{
	}
	
	
	// Called when an item received
	public override void OnItemReceived(SubTileItem item)
	{
		if (item.GetType() == typeof(SubTileItemTrainTarget) )
		{
			m_FinalDestinationReached = true;
		}
	}

}
