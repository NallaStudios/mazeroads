﻿using UnityEngine;
using System.Collections;

public class LevelBoard : MonoBehaviour {
	
	public enum InputFlags
	{
		eLeftMousePressed = 1 << 0,
		eLeftMouseDrag = 1 << 1,
	}
	
	public class StaticSubTileData
	{
		public bool m_SubTileEnabled;
		
		public StaticSubTileData()
		{
			m_SubTileEnabled = true;
		}
	}
	
	public delegate bool CanSelectTile(Tile tile);
	
	public readonly float TileSize = 3.0f;
	
	private readonly float m_TileTranslateSpeed = 40;
	private readonly float m_TileRotateSpeed = 720;
	
	public Board m_CurrentBoard = null;
	
	private Board.TileData[] m_GameTiles;
	
	private StaticSubTileData[] m_StaticDataGrid;
	
	private int m_BoardSubtileRowCount;
	private int m_BoardSubtileColCount;
	
	public Tile m_SelectedTile;
	private Tile m_DragTile;
	private Vector3 m_DragSnapPos;
	private Vector3 m_NonDragSnapPos;
	private Index m_DragTarget;
	private float m_TotalTileMoveDist;
	
	
	public CanSelectTile m_CanSelectTile;
	
	
	private GameObject m_SelectedGizmoGO;
	private SelectionGizmo m_SelectedGizmo;
	
	private ArrayList m_TileTransitions = null;
	private ArrayList m_TransToRemove = null;
	
	private bool m_GizmoOperationDone = false;
	private bool m_CamreaOperationsDone = false;

	
	private Flag m_Flag;
	
	private Vector3 m_LastMousePos;
	private Vector3 m_LastPlaneIntersectPos;
	
	private Bounds m_LevelBounds;
	private Bounds m_CameraPosBound;
	
	private GameObjectives m_GameObjectives;
	
#if UNITY_EDITOR
	private bool m_StartChoosen;
	private Index m_StartPos;
	private ArrayList m_LineGO;
#endif
	
	
	
	public Board.TileData[] GameTiles
	{
		get
		{
			return m_GameTiles;
		}
	}
	
	public int BoardSubtileRowCount
	{
		get
		{
			return m_BoardSubtileRowCount;
		}
	}
	
	public int BoardSubtileColCount
	{
		get
		{
			return m_BoardSubtileColCount;
		}
	}
	
	public GameObjectives GameObjectives
	{
		get
		{
			return m_GameObjectives;
		}
	}
	
	public void DestroyObjectives()
	{
		GameObject.Destroy(m_GameObjectives.gameObject);
		m_GameObjectives = null;
	}

	// Use this for initialization
	void Start () {
		m_TileTransitions = new ArrayList();
		m_TransToRemove = new ArrayList();
		
		m_Flag = new Flag(0);
		
#if UNITY_EDITOR
		m_StartChoosen = false;
#endif
		
		m_GizmoOperationDone = false;
	}
	
	// Update is called once per frame
	void Update () {
		
#if UNITY_EDITOR
		//DebugDrawBresenhamLine();
#endif
		
		if (Input.GetMouseButtonDown(1))
			RemoveSelection();
		
		if (m_TileTransitions.Count > 0)
		{
			foreach (TransformTransition trans in m_TileTransitions)
			{
				trans.Update(Time.deltaTime);
				if (trans.TransitionCompleted())
				{
					m_TransToRemove.Add(trans);
				}
			}
			
			foreach (TransformTransition trans in m_TransToRemove)
			{
				m_TileTransitions.Remove(trans);
			}
			
			m_TransToRemove.Clear();
		}
		else
		{
			if (Input.GetMouseButtonDown(0))
			{
				m_Flag.SetFlag((long)InputFlags.eLeftMousePressed);
				m_LastMousePos = Input.mousePosition;
				m_GizmoOperationDone = false;
				m_TotalTileMoveDist = 0.0f;
				DoGizmoOperation();
				if (!m_GizmoOperationDone)
					SelecDragtTile(m_LastMousePos);
				m_CamreaOperationsDone = false;
			}
			
			if (m_Flag.CheckFlag((long)InputFlags.eLeftMousePressed))
			{
				Vector3 mousePos = Input.mousePosition;
				
				if (mousePos != m_LastMousePos)
				{
					m_Flag.SetFlag((long)InputFlags.eLeftMouseDrag);
					//m_GizmoOperationDone = true;
					
					if ( Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl) )
					{
						UpdateCameraPosition();
					}
					else
					{
						MoveTileAlongPos(m_LastMousePos,mousePos);
					}
				}
				
				m_LastMousePos = mousePos;
			}
			
			if (Input.GetMouseButtonUp(0))
			{
				if (!m_Flag.CheckFlag((long)InputFlags.eLeftMouseDrag))
				{
					if (!m_GizmoOperationDone)
					{
						SelectTile(Input.mousePosition);
					
						if (m_SelectedTile == null || m_SelectedTile.TileType == Tile.Type.eNone)
							GameObject.Destroy(m_SelectedGizmoGO);
					}
					
					if (m_DragTile != null)
					{
						TransformTransition trans = new TransformTransition();
						trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,m_DragTile.transform,m_DragSnapPos,new Vector3(m_TileTranslateSpeed,0,0));
						m_TileTransitions.Add(trans);
						m_DragTile = null;
						m_DragTarget = null;
					}
				}
				else
				{
					if (m_DragTile != null)
					{
						TransformTransition trans = new TransformTransition();
						trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,m_DragTile.transform,m_DragSnapPos,new Vector3(m_TileTranslateSpeed,0,0));
						m_TileTransitions.Add(trans);
						m_DragTile = null;
						m_DragTarget = null;
						RemoveSelection();
					}
					
					if (!m_CamreaOperationsDone && m_TotalTileMoveDist <= 0.5f)
					{
						SelectTile(Input.mousePosition);
					
						if (m_SelectedTile == null || m_SelectedTile.TileType == Tile.Type.eNone)
							GameObject.Destroy(m_SelectedGizmoGO);
					}
				}
				
				m_Flag.ClearFlag((long)InputFlags.eLeftMousePressed);
				m_Flag.ClearFlag((long)InputFlags.eLeftMouseDrag);
				m_CamreaOperationsDone = false;
			}
			
			UpdateCameraZoom();
		}
	}
	
	private void CalculateLevelBounds()
	{
		Tile startTile = GetGameTile(0,0).m_Tile;
		Tile endTile = GetGameTile(m_CurrentBoard.m_RowCount-1,m_CurrentBoard.m_ColCount-1).m_Tile;
		
		Vector3 startTilePos = startTile.transform.position;
		Vector3 endTilePos = endTile.transform.position;
		
		Vector3 size = (endTilePos - startTilePos);
		
		size.x = Mathf.Abs(size.x);
		size.y = Mathf.Abs(size.y);
		size.z = Mathf.Abs(size.z);
		
		size += new Vector3(Tile.SubTileRowCount,1,Tile.SubTileColCount);
		
		m_LevelBounds = new Bounds(transform.position - new Vector3(0.0f,0.5f,0.0f),size);
		
		//m_LevelBounds.center = transform.position;
		
		/*float width = Screen.width;
		float height = Screen.height;
		
		Ray rayMin = Game.Instance.m_Camera.ScreenPointToRay(Vector3.zero);
		Ray rayMax = Game.Instance.m_Camera.ScreenPointToRay(new Vector3(width,0.0f,height) );
		
		Plane plane = new Plane(new Vector3(0.0f,1.0f,0.0f),m_LevelBounds.center);
		
		float outDistMin, outDistMax;
		if( plane.Raycast(rayMin, out outDistMin) && plane.Raycast(rayMax, out outDistMax) )
		{
			Vector3 min = rayMin.GetPoint(outDistMin);
			Vector3 max = rayMax.GetPoint(outDistMax);
			m_CameraPosBound = new Bounds(m_LevelBounds.center,max - min);
		}*/
	}
	
	private void UpdateCameraPosition()
	{
		Ray rayPrv = Game.Instance.m_Camera.ScreenPointToRay(m_LastMousePos);
		Ray rayCurr = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
		Plane plane = new Plane(new Vector3(0.0f,1.0f,0.0f),m_LevelBounds.center);
		
		float outDistPrv, outDistCurr;
		if( plane.Raycast(rayPrv, out outDistPrv) && plane.Raycast(rayCurr, out outDistCurr) )
		{
			Vector3 prvPos = rayPrv.GetPoint(outDistPrv);
			Vector3 currPos = rayCurr.GetPoint(outDistCurr);
			
			Vector3 diff = prvPos - currPos;
			
			//Debug.Log("Diff : " + diff);
			
			Vector3 tempPos = Game.Instance.m_Camera.transform.position; 
			tempPos.z += diff.z;
			tempPos.x += diff.x;
			Game.Instance.m_Camera.transform.position = tempPos;
			
			m_CamreaOperationsDone = true;
		}
		
		ClampCameraTranslate();
	}
	
	private void UpdateCameraZoom()
	{
		Vector3 position = Game.Instance.m_Camera.transform.position; 
		
		float newY = position.y - Input.GetAxis("Mouse ScrollWheel") * 4.0f;
		
		if (newY > 10.0f)
			position.y = newY;
		else
			position.y = 10.0f;
		
		Game.Instance.m_Camera.transform.position = position;
				
		DebugDraw.DrawBound(m_LevelBounds,Color.red);
		//DebugDraw.DrawBoundGameView(m_CameraPosBound);
		
		ClampCameraZoom();
		ClampCameraTranslate();
	}
	
	public void StartClampCamera()
	{
		CalculateLevelBounds();
		ClampCameraZoom();
		ClampCameraTranslate();
	}
	
	private void ClampCameraZoom()
	{
		Vector3 projMin = Game.Instance.m_Camera.WorldToViewportPoint(m_LevelBounds.min);
		Vector3 projMax = Game.Instance.m_Camera.WorldToViewportPoint(m_LevelBounds.max);
		
		float diffX = projMax.x - projMin.x;
		float diffZ = projMax.y - projMin.y;//after projection
		
		float zoom = (diffX < diffZ)?diffX:diffZ;
		
		float zValue = (projMax.z<projMin.z)?projMax.z:projMin.z;
		
		if (zoom < 1.0f)
		{
			//float prvdistFloat = Game.Instance.m_Camera.transform.position.y;
			float camDist = zValue * zoom;
			Vector3 camPos = Game.Instance.m_Camera.transform.position;
			camPos.y = camDist;
			Game.Instance.m_Camera.transform.position = camPos;
		}
	}
	
	private void ClampCameraTranslate()
	{
		Vector3 camPos = Game.Instance.m_Camera.transform.position;
		Plane plane = new Plane(m_LevelBounds.center,Vector3.zero);
		
		Vector3 projMin = Game.Instance.m_Camera.WorldToViewportPoint(m_LevelBounds.min);
		Vector3 projMax = Game.Instance.m_Camera.WorldToViewportPoint(m_LevelBounds.max);
		
		if (projMin.x > 0.0f || projMin.y > 0.0f)
		{
			Ray minRay = Game.Instance.m_Camera.ViewportPointToRay(Vector3.zero);		
			float minDist = 0.0f;
			plane.Raycast(minRay,out minDist);
			
			Vector3 minPoint = minRay.GetPoint(minDist);
			
			Vector3 diff = m_LevelBounds.min - minPoint;
			
			if (projMin.x > 0.0f)
				camPos.x += diff.x;
			if (projMin.y > 0.0f)
				camPos.z += diff.z;
		}
		
		if (projMax.x < 1.0f || projMax.y < 1.0f)
		{
			Ray maxRay = Game.Instance.m_Camera.ViewportPointToRay(new Vector3(1.0f,1.0f,0.0f));		
			float maxDist = 0.0f;
			plane.Raycast(maxRay,out maxDist);
			
			Vector3 maxPoint = maxRay.GetPoint(maxDist);
			
			Vector3 diff = m_LevelBounds.max - maxPoint;
			
			if (projMax.x < 1.0f)
				camPos.x += diff.x;
			if (projMax.y < 1.0f)
				camPos.z += diff.z;
		}
		
		Game.Instance.m_Camera.transform.position = camPos;
	}
	
	private void DoGizmoOperation()
	{
		Ray ray = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
				
		SelectionGizmo.GizmoType type;
		if (m_SelectedGizmoGO != null && (type = m_SelectedGizmo.CheckGizmoClicked(ray)) != SelectionGizmo.GizmoType.eCount )
		{
			if (m_CanSelectTile != null && !m_CanSelectTile(m_SelectedTile))
			{
				//RemoveSelection();
			}
			else
			{
				DoGizmoOperation(type);
				m_GizmoOperationDone = true;
			}
		}
	}
	
	private void MoveTileAlongPos(Vector3 prvMousePos, Vector3 currMousePos)
	{
		if (m_DragTile == null)
			return;
		
		Ray rayPrv = Game.Instance.m_Camera.ScreenPointToRay(prvMousePos);
		Ray rayCurr = Game.Instance.m_Camera.ScreenPointToRay(currMousePos);
		Plane plane = new Plane(new Vector3(0.0f,1.0f,0.0f),Vector3.zero);
		
		float outDistPrv, outDistCurr;
		if( plane.Raycast(rayPrv, out outDistPrv) && plane.Raycast(rayCurr, out outDistCurr) )
		{
			Vector3 prvPos = rayPrv.GetPoint(outDistPrv);
			Vector3 currPos = rayCurr.GetPoint(outDistCurr);
			
			Vector3 diff = new Vector3(currPos.x - prvPos.x,0.0f, currPos.z - prvPos.z);
			
			Index dIndex = m_DragTile.Index;
			
			if (m_DragTarget == null)
			{
				m_DragTarget = new Index();
			
				if ( Mathf.Abs(diff.x) > Mathf.Abs(diff.z))
				{
					int dir = (diff.x < 0.0f)?-1:1;
					m_DragTarget.row = dIndex.row;
					m_DragTarget.col = dIndex.col + dir;
					diff.z = 0.0f;
				}
				else
				{
					int dir = (diff.z < 0.0f)?1:-1;
					m_DragTarget.row = dIndex.row + dir;
					m_DragTarget.col = dIndex.col;
					diff.x = 0.0f;
				}
			}
			else
			{
				if (m_DragTarget.row == dIndex.row)
				{
					int dir = (diff.x < 0.0f)?-1:1;
					m_DragTarget.row = dIndex.row;
					m_DragTarget.col = dIndex.col + dir;
					diff.z = 0.0f;
				}
				else
				{
					int dir = (diff.z < 0.0f)?1:-1;
					m_DragTarget.row = dIndex.row + dir;
					m_DragTarget.col = dIndex.col;
					diff.x = 0.0f;
				}
			}
			
			Board.TileData targetTile = GetGameTile(m_DragTarget.row,m_DragTarget.col);
			
			//Debug.Log("Moving Diff : " + m_DragTile.name + "  Row : " + dIndex.row + " Col : " + dIndex.col);
			
			if (targetTile != null && targetTile.m_Tile.TileType == Tile.Type.eNone)
			{
				Vector3 distVecFromDragSnapPos = m_DragTile.transform.position - m_DragSnapPos;
				float distFromDragSnapPos = distVecFromDragSnapPos.magnitude;
				
				Vector3 distVecFromTarget = targetTile.m_Tile.transform.position - m_DragTile.transform.position;
				float distFromTarget = distVecFromTarget.magnitude;
				
				//Debug.Log("Distance from target : " + distFromTarget + "  Distance from drag snap pos : " + distFromDragSnapPos);
				
				if (distFromTarget <= distFromDragSnapPos)
				{
					//m_NonDragSnapPos = 
					SwapGameTiles(m_DragTile.TileData,targetTile,false);
					Vector3 tempPos = targetTile.m_Tile.transform.position;
					targetTile.m_Tile.transform.position = m_DragSnapPos;
					m_DragSnapPos = tempPos;
					m_DragTarget = null;
				}
				
				//Debug.Log("Moving Diff : " + diff);
				
				m_DragTile.transform.position = m_DragTile.transform.position + diff;
				
				m_TotalTileMoveDist += diff.magnitude;
			}
			else
			{
				Vector3 newPos = m_DragTile.transform.position + diff;
				
				m_TotalTileMoveDist += diff.magnitude;
				
				Vector3 prvDist = m_DragSnapPos - m_DragTile.transform.position;
				Vector3 newDist = m_DragSnapPos - newPos;
				
				if (prvDist.sqrMagnitude > newDist.sqrMagnitude)
				{
					m_DragTile.transform.position = newPos;
					
					float EPSILON = 0.1f;
					
					//Debug.Log("Distance : " + newDist.magnitude);
					if (newDist.magnitude <= EPSILON)
					{
						m_DragTile.transform.position = m_DragSnapPos;
						m_DragTarget = null;
						//m_DragSnapPos = 
						//m_DragTile = null;
						//SelecDragtTile(Input.mousePosition);
						//Debug.Log("Snapped :");
						//m_TotalTileMoveDist = 0.0f;
					}
				}
				else
				{
					m_DragTarget = null;
				}
			}
		}
	}
	
	private void SelecDragtTile(Vector3 mousePos)
	{
		Ray ray = Game.Instance.m_Camera.ScreenPointToRay(mousePos);
		
		m_DragTile = null;
						
		RaycastHit[] objectsHit = Physics.RaycastAll(ray);
		
		for (int i = 0 ; i < objectsHit.Length ; i++)
		{
			GameObject hitGO = objectsHit[i].transform.gameObject;
			Tile tile = hitGO.GetComponent<Tile>();
			
			if (tile != null && tile.TileType != Tile.Type.eNone && tile.TileType != Tile.Type.eStatic && m_CanSelectTile(tile))
			{
				m_DragTile = tile;
				m_DragSnapPos = m_DragTile.transform.position;
				//Debug.Log("Drag Tile Selected..");
			}
		}
	}
	
	private void SelectTile(Vector3 mousePos)
	{
		Ray ray = Game.Instance.m_Camera.ScreenPointToRay(mousePos);
		
		m_SelectedTile = null;
						
		RaycastHit[] objectsHit = Physics.RaycastAll(ray);
		
		for (int i = 0 ; i < objectsHit.Length ; i++)
		{
			GameObject hitGO = objectsHit[i].transform.gameObject;
			Tile tile = hitGO.GetComponent<Tile>();
			
			if (tile != null)
			{
				
				m_SelectedTile = tile;
				if (m_SelectedGizmoGO == null)
				{
					m_SelectedGizmoGO = GameObject.Instantiate(Game.Instance.GizmoPrefabRef,hitGO.transform.position,Quaternion.Euler(new Vector3(90.0f,00.0f,0.0f))) as GameObject;
					m_SelectedGizmo = m_SelectedGizmoGO.GetComponent<SelectionGizmo>();
				}
				else
					m_SelectedGizmoGO.transform.position = hitGO.transform.position;
				break;
			}
		}
	}
	
	
	public void RemoveSelection()
	{
		m_Flag.Reset();
		m_SelectedTile = null;
		if (m_SelectedGizmoGO != null)
			GameObject.Destroy(m_SelectedGizmoGO);
	}
	
	public void LoadCurrentBoard()
	{
		if (m_CurrentBoard != null)
		{
			int totalCount = m_CurrentBoard.m_RowCount * m_CurrentBoard.m_ColCount; 
			m_GameTiles = new Board.TileData[totalCount];
			
			float halfWidth = 1.5f * (m_CurrentBoard.m_ColCount-1);
			float halfHeight = 1.5f * (m_CurrentBoard.m_RowCount-1);
			
			Vector3 pos = Vector3.zero;
					
			pos.x = -halfWidth;
			pos.z = halfHeight;
			
			for (int i = 0 ; i < m_CurrentBoard.m_RowCount ; i++)
			{
				for (int j = 0 ; j < m_CurrentBoard.m_ColCount ; j++)
				{
					int index = i * m_CurrentBoard.m_ColCount + j;
					
					m_GameTiles[index] = new Board.TileData();
					
					m_GameTiles[index].m_Tile = Instantiate(m_CurrentBoard.m_Tiles[index].m_Tile, pos,Quaternion.identity) as Tile;
					
					m_GameTiles[index].m_Tile.gameObject.transform.parent = transform;
					
					m_GameTiles[index].m_Tile.Index = new Index(i,j);
					
					m_GameTiles[index].m_Tile.UpdateSubTileIndices();
					
					m_GameTiles[index].m_Orientation = m_CurrentBoard.m_Tiles[index].m_Orientation;
					
					m_GameTiles[index].m_TileType = m_CurrentBoard.m_Tiles[index].m_TileType;
					
					m_GameTiles[index].m_Tile.TileData = m_GameTiles[index];
					
					m_GameTiles[index].m_Tile.CurrentLevelBoard = this;
					
					m_GameTiles[index].m_Tile.UpdateTileBasedOnOrientation(true);
					m_GameTiles[index].m_Tile.CreateItems(m_CurrentBoard.m_Tiles[index]);
					
					pos.x += TileSize;
					
				}
				pos.x = -halfWidth;
				pos.z -= TileSize;
			}
			
			int totalSubTileCount = totalCount * Tile.SubTileRowCount * Tile.SubTileColCount;
			m_StaticDataGrid = new StaticSubTileData[totalSubTileCount];
			
		
			for (int i = 0 ; i < totalSubTileCount ; i++)
			{
				m_StaticDataGrid[i] = new StaticSubTileData();
			}
		}
		
		m_BoardSubtileRowCount = Tile.SubTileRowCount * m_CurrentBoard.m_RowCount;
		m_BoardSubtileColCount = Tile.SubTileColCount * m_CurrentBoard.m_ColCount;
		
		string name = "Boards/" + "Objectives" + m_CurrentBoard.name;
		UnityEngine.Object prefab = Resources.Load(name,typeof(GameObject));
		
		if (prefab != null)
		{
			GameObject objGO = GameObject.Instantiate(prefab) as GameObject;
			m_GameObjectives = objGO.GetComponent<GameObjectives>();
			m_GameObjectives.transform.parent = transform;
		}
	}
	
	public void DestroyBoard()
	{
		if (m_GameTiles == null)
			return;
		
		for (int i = 0 ; i < m_GameTiles.Length ; i++)
		{
			m_GameTiles[i].m_Tile.gameObject.SetActive(false);
			GameObject.DestroyImmediate(m_GameTiles[i].m_Tile.gameObject);
		}
		
		m_GameTiles = null;
	}
	
	public SubTile GetSubtileFromBoard(int row, int col, out Tile parentTile)
	{
		int tileRow = (int)row/Tile.SubTileRowCount;
		int tileCol = (int)col/Tile.SubTileColCount;
		
		parentTile = m_GameTiles[tileRow * m_CurrentBoard.m_ColCount + tileCol].m_Tile;
		
		int subTileRow = (int)row%Tile.SubTileRowCount;
		int subTileCol = (int)col%Tile.SubTileColCount;
		
		
		return parentTile.GameSubTiles[subTileRow * Tile.SubTileRowCount + subTileCol];
	}
	
	public Board.TileData GetGameTile(int row, int col)
	{
		if (row < 0 || col < 0 ||row >= m_CurrentBoard.m_RowCount || col >= m_CurrentBoard.m_ColCount)
			return null;
		else
			return GameTiles[row * m_CurrentBoard.m_ColCount + col];
	}
	
	public Board.TileData GetBoardTile(int row, int col)
	{
		return m_CurrentBoard.m_Tiles[row * m_CurrentBoard.m_ColCount + col];
	}
	
	public void SwapGameTiles(Board.TileData first, Board.TileData second, bool transition = true)
	{
		GameTiles[first.m_Tile.Index.row * m_CurrentBoard.m_ColCount + first.m_Tile.Index.col] = second;
		GameTiles[second.m_Tile.Index.row * m_CurrentBoard.m_ColCount + second.m_Tile.Index.col] = first;
		
		Index temp = second.m_Tile.Index;
		second.m_Tile.Index = first.m_Tile.Index;
		first.m_Tile.Index = temp;
		
		//Vector3 tempPos = first.m_Tile.transform.position;
		//first.m_Tile.transform.position = second.m_Tile.transform.position;
		//second.m_Tile.transform.position = tempPos;
		
		if (transition)
		{
			TransformTransition trans = new TransformTransition();
			trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,first.m_Tile.transform,second.m_Tile.transform.position,new Vector3(m_TileTranslateSpeed,0,0));
			m_TileTransitions.Add(trans);
			
			trans = new TransformTransition();
			trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,second.m_Tile.transform,first.m_Tile.transform.position,new Vector3(m_TileTranslateSpeed,0,0));
			m_TileTransitions.Add(trans);
		}
	}
	
	private void DoGizmoOperation(SelectionGizmo.GizmoType type)
	{
		if (m_SelectedTile == null)
			return;
		
		switch(type)
		{
		case SelectionGizmo.GizmoType.eLeft:
			Index currIndex = m_SelectedTile.Index;
			Board.TileData selectedTileData = GetGameTile(currIndex.row,currIndex.col);
			Board.TileData tileToSwap = GetGameTile(currIndex.row,currIndex.col - 1);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapGameTiles(tileToSwap,selectedTileData);
					//m_SelectedGizmoGO.transform.position = m_SelectedTile.transform.position;
					
					TransformTransition trans = new TransformTransition();
					trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,m_SelectedGizmoGO.transform,tileToSwap.m_Tile.transform.position,new Vector3(m_TileTranslateSpeed,0,0));
					m_TileTransitions.Add(trans);
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eRight:
			currIndex = m_SelectedTile.Index;
			selectedTileData = GetGameTile(currIndex.row,currIndex.col);
			tileToSwap = GetGameTile(currIndex.row,currIndex.col + 1);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapGameTiles(tileToSwap,selectedTileData);
					//m_SelectedGizmoGO.transform.position = m_SelectedTile.transform.position;
					
					TransformTransition trans = new TransformTransition();
					trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,m_SelectedGizmoGO.transform,tileToSwap.m_Tile.transform.position,new Vector3(m_TileTranslateSpeed,0,0));
					m_TileTransitions.Add(trans);
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eUp:
			currIndex = m_SelectedTile.Index;
			selectedTileData = GetGameTile(currIndex.row,currIndex.col);
			tileToSwap = GetGameTile(currIndex.row-1,currIndex.col);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapGameTiles(tileToSwap,selectedTileData);
					//m_SelectedGizmoGO.transform.position = m_SelectedTile.transform.position;
					
					TransformTransition trans = new TransformTransition();
					trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,m_SelectedGizmoGO.transform,tileToSwap.m_Tile.transform.position,new Vector3(m_TileTranslateSpeed,0,0));
					m_TileTransitions.Add(trans);
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eDown:
			currIndex = m_SelectedTile.Index;
			selectedTileData = GetGameTile(currIndex.row,currIndex.col);
			tileToSwap = GetGameTile(currIndex.row+1,currIndex.col);
			if (tileToSwap != null)
			{
				if (tileToSwap.m_Tile.TileType == Tile.Type.eNone && selectedTileData.m_Tile.TileType != Tile.Type.eStatic)
				{
					SwapGameTiles(tileToSwap,selectedTileData);
					//m_SelectedGizmoGO.transform.position = m_SelectedTile.transform.position;
					
					TransformTransition trans = new TransformTransition();
					trans.StartTransitioning(TransformTransition.TransitionType.eTranslate,m_SelectedGizmoGO.transform,tileToSwap.m_Tile.transform.position,new Vector3(m_TileTranslateSpeed,0,0));
					m_TileTransitions.Add(trans);
				}
			}
			break;
			
		case SelectionGizmo.GizmoType.eRotateClockWise:
			currIndex = m_SelectedTile.Index;
			RotateTileClockwise(currIndex);
			break;
			
		case SelectionGizmo.GizmoType.eRotateAntiClockWise:
			currIndex = m_SelectedTile.Index;
			RotateTileAntiClockwise(currIndex);
			break;
			
		default:
			break;
		}
	}
	
	public void RotateTileClockwise(Index index)
	{
		Board.TileData data = GetGameTile(index.row,index.col);
		
		if (data.m_TileType == Tile.Type.eStatic)
			return;
		
		data.m_Orientation += 1;
		
		if (data.m_Orientation > Tile.OrientationType.eOrientation_270)
			data.m_Orientation = Tile.OrientationType.eOrientation_0;
		data.m_Tile.UpdateTileBasedOnOrientation(false);
		
		TransformTransition trans = new TransformTransition();
		trans.StartTransitioning(TransformTransition.TransitionType.eRotate,data.m_Tile.transform,new Vector3(0.0f,Tile.Angles[(int)data.m_Orientation],0.0f),new Vector3(m_TileRotateSpeed,0,0));
		m_TileTransitions.Add(trans);
	}
	
	public void RotateTileAntiClockwise(Index index)
	{
		Board.TileData data = GetGameTile(index.row,index.col);
		
		if (data.m_TileType == Tile.Type.eStatic)
			return;
		
		data.m_Orientation -= 1;
		
		if (data.m_Orientation < 0)
			data.m_Orientation = Tile.OrientationType.eOrientation_270;
		data.m_Tile.UpdateTileBasedOnOrientation(false);
		
		TransformTransition trans = new TransformTransition();
		trans.StartTransitioning(TransformTransition.TransitionType.eRotate,data.m_Tile.transform,new Vector3(0.0f,Tile.Angles[(int)data.m_Orientation],0.0f),new Vector3(m_TileRotateSpeed,0,0));
		m_TileTransitions.Add(trans);
	}
	
	public Index ChooseSubTileIndexFromRayCollide(Ray ray)
	{
		RaycastHit[] objectsHit = Physics.RaycastAll(ray);
						
		for (int i = 0 ; i < objectsHit.Length ; i++)
		{
			GameObject hitGO = objectsHit[i].transform.gameObject;
			Tile tile = hitGO.GetComponent<Tile>();
			
			if (tile != null)
			{
				return ChooseSubTileIndexFromPosition(objectsHit[i].point,objectsHit[i].transform.position, tile.Index);
			}
		} 
		
		return null;
	}
	
	public Index ChooseSubTileIndexFrom2DPosition(Vector3 pos)
	{
		Ray newRay = new Ray(new Vector3(pos.x,20.0f,pos.z),new Vector3(0.0f,-1.0f,0.0f));
		return ChooseSubTileIndexFromRayCollide(newRay);
	}

	public Index ChooseSubTileIndexFromPosition(Vector3 objectPos, Vector3 parentPos, Index parentIndex)
	{
		// please check later it may not work if the entire board is transformed..
					
		Vector3 point = (objectPos - parentPos) + (new Vector3(TileSize,0.0f,TileSize)) * 0.5f;
				
		point.z = (float)TileSize - point.z;
		
		point.z = Mathf.Clamp(point.z,0.0f,TileSize);
						
		Index tileIndex = parentIndex;
		
		Index newInd = new Index(0,0);

		newInd.row = tileIndex.row * Tile.SubTileRowCount + (int)point.z;
		newInd.col = tileIndex.col * Tile.SubTileColCount + (int)point.x;

		
		return newInd;
	}
	
	public void GetTwoSubtileBasedOn2DPositionOnEdge(Vector3 pos, out Index subtileIndex1, out Index subtileIndex2)
	{
		subtileIndex1 = null;
		subtileIndex2 = null;
		
		Ray ray = Game.Instance.m_Camera.ScreenPointToRay(pos);
		
		RaycastHit[] objectsHit = Physics.RaycastAll(ray);
						
		for (int i = 0 ; i < objectsHit.Length ; i++)
		{
			GameObject hitGO = objectsHit[i].transform.gameObject;
			Tile tile = hitGO.GetComponent<Tile>();
			
			if (tile != null)
			{
				Tile parentTile;
				subtileIndex1 = ChooseSubTileIndexFromPosition(objectsHit[i].point,objectsHit[i].transform.position, tile.Index);
				SubTile subTile1 = GetSubtileFromBoard(subtileIndex1.row,subtileIndex1.col,out parentTile);
				Vector3 subTile1Pos = subTile1.transform.position;
				Vector3 diffWithRayCast = objectsHit[i].point - subTile1Pos;
				diffWithRayCast.y = 0.0f;
				
				if ( Mathf.Abs(diffWithRayCast.x) > Mathf.Abs(diffWithRayCast.z) )
				{
					if (diffWithRayCast.x < 0.0f)
					{
						subtileIndex2 = new Index(subtileIndex1.row,subtileIndex1.col + 1);
					}
					else
					{
						subtileIndex2 = new Index(subtileIndex1.row,subtileIndex1.col - 1);
					}
				}
				else
				{
					if (diffWithRayCast.z < 0.0f)
					{
						subtileIndex2 = new Index(subtileIndex1.row - 1,subtileIndex1.col);
					}
					else
					{
						subtileIndex2 = new Index(subtileIndex1.row + 1,subtileIndex1.col);
					}
				}
			}
		} 
	}
	
	public StaticSubTileData GetStaticSubTileData(int row, int col)
	{
		return m_StaticDataGrid[row * m_BoardSubtileColCount + col];
	}
	
	public StaticSubTileData GetStaticSubTileData(Index index)
	{
		return GetStaticSubTileData(index.row,index.col);
	}
	
#if UNITY_EDITOR
	private void DebugDrawBresenhamLine()
	{
		if (Input.GetMouseButtonDown(1))
		{
			if (!m_StartChoosen)
			{
				Ray ray = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
				
				m_StartPos = ChooseSubTileIndexFromRayCollide(ray);
				
				m_StartChoosen = true;
				
				DestroyAllLineGO();
				Plot(m_StartPos.row,m_StartPos.col);
			}
			else
			{
				Ray ray = Game.Instance.m_Camera.ScreenPointToRay(Input.mousePosition);
				
				Index endPos = ChooseSubTileIndexFromRayCollide(ray);
				
				m_StartChoosen = false;
				
				DestroyAllLineGO();
				//Bresenham.Line(m_StartPos.row,m_StartPos.col,endPos.row,endPos.col,Plot);
				
				PathFinder newFinder = new PathFinder();
				Index[] path = newFinder.FindPath(this,m_StartPos,endPos,CanMoveToTileType);
				
				foreach (Index index in path)
				{
					Plot(index.row,index.col);
				}
			}
		}
	}
	
	private bool CanMoveToTileType(SubTile.SubTileType type)
	{
		return type == SubTile.SubTileType.eRoad;
	}
	
	private bool Plot(int x, int y)
	{
		Tile parentTile;
		SubTile subTile = GetSubtileFromBoard(x,y,out parentTile);
		
		if (subTile != null)
		{
			GameObject go = subTile.gameObject;
			Vector3 pos = go.transform.position;
			pos.y += 0.1f;
			GameObject newGO = Instantiate(Game.Instance.m_Pixel,pos,Quaternion.Euler(90,0,0)) as GameObject;
			
			if (m_LineGO == null)
				m_LineGO = new ArrayList();
			m_LineGO.Add(newGO);
			
			return true;
		}
		
		return false;
	}
	
	private void DestroyAllLineGO()
	{
		if (m_LineGO != null)
		{
			foreach (GameObject go in m_LineGO)
			{
				GameObject.Destroy(go);
			}
			m_LineGO.Clear();
		}
	}
#endif
}
